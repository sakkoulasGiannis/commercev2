<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class Calculator extends Model
{
    protected $fillable = ['product_id', 'query'];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
