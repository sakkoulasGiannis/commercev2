<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'parent_id',
        'erp_id' ,
        'title',
        'name',
        'active',
        'hidden',
        'description',
        'meta_title',
        'meta_description',
        'order',
        'dynamic',
        'query',
        'mega_menu_is_active',
        'dynamic_query_is_active',
        'mega_menu_content',
        'dynamic_query_content'
    ];

    public function products()
    {

        return $this->belongsToMany(Product::class, 'product_category')
            ->whereHas('stock', function ($query) {
                $query->where([['quantity', '>', 0]]);
            });
    }
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->orderBy('order', 'asc');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->orderBy('order', 'asc');
    }

    public function subChildren (){
        return $this->children();
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }


}
