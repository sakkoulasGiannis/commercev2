<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use App\Field;
class deleteField extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'field:delete {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->deleteTable($name);
        $this->removeField($name);
        $this->deleteRecord($name);
    }

    /*
    * remove the method in the fieldtrait
    */
    public function removeField($name){

        $this->info('remove method in fieldtrait');
        $name = ucFirst($name);
//        remove file
        $fieldFile =    app_path('Fields/Field' . ucFirst($name) . '.php');
        unlink($fieldFile);

// remove method from FieldTrait
        $blockStart = 'field_'.$name;
        $blockEnd = 'end_field_'.$name;
        $filePath = app_path('Http/Traits/FieldTrait.php');
        $file = file_get_contents($filePath);
        $newFile = preg_replace('/\/\/'.$blockStart.'[\s\S]+?'.$blockEnd.'/', '', $file);
        $newFile = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $newFile);

        file_put_contents($filePath, $newFile); //Overwrite the file with the new content
        $this->info('end remove method in fieldtrait');
        return true;

    }

    public function deleteTable($name){
        Schema::dropIfExists('field_' . str_plural($name).'_product');
        Schema::dropIfExists('field_' . str_plural($name));
        $this->info('deleting tables');
    }
    public function deleteRecord($name){
        $field  = Field::where('name', $name)->first();
        if($field){
            Field::destroy($field->id);
            $this->info('destroy record');
        }

    }

}
