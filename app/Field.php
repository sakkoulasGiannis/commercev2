<?php

namespace App;

use App\Fields\Color;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{

    protected $fillable = [
        'title',
        'name',
        'type',
        'model_type',
        'positions',
        'position',
        'can_select_multiple',
        'is_read_only',
        'is_enabled',
     ];

    //@todo delete this ;;;;
    public function values($query){
        $modelType = $this->model_type;

        return $modelType::all();
        return $this->hasMany(\App\Fields\FieldWidth::class, 'field_id', 'id');
    }
}
