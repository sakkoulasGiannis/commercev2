<?php

namespace App\Http\Controllers\Api;


use App\Category;
use App\Field;
use App\Product;
use App\StockVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;

class ProductController extends Controller
{
    public function createOrUpdate(Request $request){

        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) {
            return 'Request method must be POST';
        }

        //Make sure that the content type of the POST request has been set to application/json
        $contentType = isset($_SERVER['CONTENT_TYPE']) ? trim($_SERVER['CONTENT_TYPE']) : '';
        if (strcasecmp($contentType, 'application/json') != 0) {
            // return 'Content type must be: application/json';
        }

        //Receive the RAW post data.
        $content = trim(file_get_contents('php://input'));
        Log::error('create at:' . strtotime('d-m-Y') . ' - ' . $content);


        //Attempt to decode the incoming RAW post data from JSON.
        $data = json_decode($content, true);

        if (!is_array($data)) {
            Log::info('create product error: Received content contained invalid JSON');
             return 'Received content contained invalid JSON!';
        }

        if(isset($data['id'])){
            return $this->updateProduct($data);
        }
        return $this->createProduct($data);

    }
    public function checkSKU($data)
    {
        $product = Product::where('sku', $data['sku'])->first();
        if ($product) {
            return true;
        }
        return false;
    }

    public function createProduct($data){
        if ($this->checkSKU($data)) {
            return null;
        }
        $messages = [];
        $product = new Product();
        try {

            $product->is_active = isset($data['is_active'])? $data['is_active']: 1;
            $product->setTranslation('title', 'el', $data['title']);
            $product->name = $data['name'];
            $product->sku = $data['sku'];
            $product->meta_title = $data['meta_title'];
            $product->meta_description = $data['meta_description'];
            $product->group = $data['group'];
            $product->price = $data['price'];
            $product->product_type = $data['product_type'];
            $product->special_price = $data['special_price'];
            $product->tax_class_id = $data['tax_class_id'];
            $product->is_featured = $data['is_featured'];
            $product->description = $data['description'];
            $product->save();

            if(isset($data['categories'])){
                $categories = explode(',', $data['categories']);
                $product->categories()->sync($categories);
            }elseif(isset($data['erp_categories'])){
                 $categories = explode(',', $data['erp_categories']);
                 $categories = Category::whereIn('erp_id', $categories)->pluck('id');
                 $product->categories()->sync($categories);
            }
    //        add media
            if (isset($data['media']) && count($data['media']) > 0) {

                foreach ($data['media'] as $val => $media) {
                    if (isset($media['path']) and $media['path'] != '') {
                        $product->addMediaFromUrl($media['path'])->toMediaCollection();
                    } elseif (isset($media['data']) and $media['data'] != '') {
                        $product->addMediaFromBase64($media['data'])->toMediaCollection();
                    }
                }
            } else {
                $messages = ['product will be deactivated has no images '];
                $product->is_active = 0;
            }
    //       add attributes
            if (isset($data['attributes']) && count($data['attributes']) >= 0) {
                foreach($data['attributes'] as $key => $a){

                    if(!Schema::hasTable('field_'. Str::plural($key))){
                        continue;
                    }

                    $model = "\\App\\Fields\\Field".ucfirst($key);

                    // check for multiple values
                    if( strpos($a, ',') !== false ) {
                        $multipleAttribures = explode(',', $a);
                        $mas = [];
                        foreach($multipleAttribures as $ma){
                            $ma = trim($ma);
                            if($value = $model::where('title', $ma)->first() ){
                                $mas[] = $value->id;
                            }else{
                                $value = $model::create(['title' => $ma, 'name'=>str_slug($ma, '-')]);
                                $mas[] = $value->id;
                            }
                        }
                        $product->$key()->sync($mas);
                        // add signle attribute
                    }else{
                        if($value = $model::where('title', $a)->first() ){
                            $product->$key()->sync([$value->id]);
                        }else{
                            $value = $model::create(['title' => $a, 'name'=>str_slug($a, '-')]);
                            $product->$key()->sync([$value->id]);
                        }
                    }



                }
            }
    //      add stock

            if (isset($data['stock']) && count($data['stock']) > 0) {
            $stock = $data['stock'];
            for($i=0; $i<count($stock);$i++){

                $createdStock = $product->stock()->updateOrCreate(
                    ['sku' => (isset($stock[$i]['sku']) ? $stock[$i]['sku'] : null), 'product_id'=> $product->id],
                    [
                        'sku'=>$stock[$i]['sku'],
                        'ean'=> (isset($stock[$i]['ean']))?$stock[$i]['ean']:null,
                        'barcode'=> (isset($stock[$i]['barcode']))?$stock[$i]['barcode']:null,
                        'price'=> (isset($stock[$i]['price']))?$stock[$i]['price']:null,
                        'quantity'=>(integer)$stock[$i]['quantity']
                    ]
                );



                if (isset($stock[$i]['variations']) && count($stock[$i]['variations']) > 0) {
                    foreach ($stock[$i]['variations'] as  $vkey => $variation) {


                        $model = "\\App\\Fields\\Field".ucfirst($variation['name']);
                         $fieldValue = $model::where('title', $variation['value'])->first();
                        if(!$fieldValue){
                            $fieldValue = $model::create(['title' => $variation['value'], 'name'=>str_slug($variation['value'], '-')]);
                        }


                        $field = Field::where('name', $variation['name'])->first();
                        StockVariation::create([
                            'stock_id'=>$createdStock->id,
                            'field_id'=>$field->id,
                            'model_type' => "\App\Fields\Field".ucFirst($variation['name']),
                            'model_id'=> $fieldValue->id
                        ]);

                    }


                }


            }
        }
            Log::notice('create product: ' . $product->id);
            return $product->id;
        } catch (\Exception $e) {
            Product::destroy($product->id);
            Log::error('create product error: ' . $e->getMessage());
            return $e->getMessage();
        }


    }
    public function updateProduct($data){
        $product = Product::where('id', $data['id'])->first();
        if(!$product){
            Log::error('Update Product Error: product with id ' . $product->id.' not exist!');
        }
        $product->is_active = isset($data['is_active'])? $data['is_active']: 0;
        $product->setTranslation('title', 'el', $data['title']);
        $product->name = $data['name'];
        $product->sku = $data['sku'];
        $product->meta_title = $data['meta_title'];
        $product->meta_description = $data['meta_description'];
        $product->group = $data['group'];
        $product->product_type = $data['product_type'];
        $product->price = $data['price'];
        $product->special_price = $data['special_price'];
        $product->tax_class_id = $data['tax_class_id'];
        $product->is_featured = $data['is_featured'];
        $product->description = $data['description'];
        $product->save();
        if (isset($data['attributes']) && count($data['attributes']) >= 0) {
            foreach($data['attributes'] as $key => $a){
                $model = "\\App\\Fields\\Field".ucfirst($key);
                if($value = $model::where('title', $a)->first() ){
                    $product->$key()->sync([$value->id]);
                }else{
                    $value = $model::create(['title' => $a, 'name'=>str_slug($a, '-')]);
                    $product->$key()->sync([$value->id]);
                }
            }
        }
        if (isset($data['stock']) && count($data['stock']) > 0) {
            $stock = $data['stock'];
            for($i=0; $i<count($stock);$i++){

                $createdStock = $product->stock()->updateOrCreate(
                    ['sku' => (isset($stock[$i]['sku']) ? $stock[$i]['sku'] : null), 'product_id'=> $product->id],
                    [
                        'sku'=>$stock[$i]['sku'],
                        'ean'=> (isset($stock[$i]['ean']))?$stock[$i]['ean']:null,
                        'barcode'=> (isset($stock[$i]['barcode']))?$stock[$i]['barcode']:null,
                        'price'=> (isset($stock[$i]['price']))?$stock[$i]['price']:null,
                        'quantity'=>(integer)$stock[$i]['quantity']
                    ]
                );




                if (isset($stock[$i]['variations']) && count($stock[$i]['variations']) >= 0) {
                    foreach ($stock[$i]['variations'] as  $vkey => $variation) {

                        $model = "\\App\\Fields\\Field".ucfirst($variation['name']);
                        $fieldValue = $model::where('title', $variation['value'])->first();
                        if($fieldValue == null){
                            $fieldValue = $model::create(['title' => $variation['value'], 'name'=>str_slug($variation['value'], '-')]);
                        }


                        $field = Field::where('name', $variation['name'])->first();
                        StockVariation::create([
                            'stock_id'=>$createdStock->id,
                            'field_id'=>$field->id,
                            'model_type' => "\App\Fields\Field".ucFirst($variation['name']),
                            'model_id'=> $fieldValue->id
                        ]);

                    }
                }
            }
        }


        if (isset($data['media']) && count($data['media']) > 0) {
            foreach ($data['media'] as $val => $media) {
                if (isset($media['path']) and $media['path'] != '') {
                    $product->addMediaFromUrl($media['path'])->toMediaCollection();
                } elseif (isset($media['data']) and $media['data'] != '') {
                    $product->addMediaFromBase64($media['data'])->toMediaCollection();
                }
            }
        } else {
            $messages = ['product will be deactivated has no images '];
        }

        return "Product has been updated";
    }
}
