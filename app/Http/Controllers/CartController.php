<?php

namespace App\Http\Controllers;

//region use
use App\Product;
use App\Stock;
use App\Http\Requests\CheckoutRequest;
use App\Mail\NewOrder;
use App\Order;
use App\Payment;
use App\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Courier;
use App\Voucher;
use Carbon\Carbon;

use Darryldecode\Cart;
use function GuzzleHttp\Psr7\str;
use App\Traits\NationalBankOfGreeceTrait;

class CartController extends Controller
{
    use NationalBankOfGreeceTrait;
    /**
     * cart page
     * @return $this
     */
    public function index()
    {

        // return \Cart::getTotal();
        $conditions = \Cart::getConditions();
        $items = $this->cartItems();
        $cartDetails = $this->cartDetails();
        return view(env('THEME') . '.cart')->with(compact('items', 'cartDetails', 'conditions'));
    }

    /**
     * checkout page
     * @return $this
     */
    public function checkout()
    {
         $couriers = [];
        $cartItems = $this->cartItems();

        $cartDetails = $this->cartDetails();
        $conditions = \Cart::getConditions();
        $payments = [];

        return view(env('THEME') . '.checkout')->with(compact('cartItems', 'conditions', 'cartDetails', 'payments', 'couriers'));
    }

    public function completed()
    {


        return view(env('THEME') . '.success_checkout');

        if (\Session::has('order_id')) {
            $order = \Session::get('order_id');
            $order = Order::findOrFail($order);

        } else {
            \Session::flash('flash_message', __('No order found'));
            return redirect(route('home'));
        }
    }

    /**
     * submit checkout form
     * @param CheckoutRequest $request
     * @return mixed
     */
    public function Complete(Request $request)
    {



        $validatedData = \Validator::make($request->all(), [
            'email' => 'required|email',
            'shipping_id' => 'required',
            'payment_id' => 'required',
            'billing_name' => 'required',
            'billing_lastname' => 'required',
            'billing_country_id' => 'required',
            'billing_country' => 'required',
        ]);



        if (\Cart::getContent()->count() <= 0) {
            abort(404);
        }


        /**
         * get cart details
         */
         $cartDetails = $this->cartDetails();

        /**
         * set invoice id
         * @todo there is a laraver function for this command
         */
         $invoice_id = Order::count() + 1;

        // check payment module
//         $payment = $this->checkPayment($request);

        $order = [
            'order' => [
                'order_num' => $invoice_id,
                'ip' => $_SERVER['REMOTE_ADDR'],
//                'comment' => $request->comment,
                'slug' => substr(md5(rand()), 0, 100) . time(), // order random slug
            ]
        ];



//        return $request->all();
//        return $request->get('payment_method') ;
        $status_id = 'pending';
        if($request->get('payment_id') == 'cod'){
            $shipping_id = 1;
            $payment_id = 1;
            $status_id = 1;
        }


        $order['details'] = array(
            'status_id' => $status_id,
            'invoice_id' => null, //@todo check shipping
            'payment_id' => $payment_id,
            'shipping_id' =>  $shipping_id, //@todo check shipping
            'total' => $cartDetails['total'],
            'sub_total' => $cartDetails['sub_total'],
            'billing_name' => request()->billing_name,
            'billing_lastname' => request()->billing_lastname,
            'billing_country_id' => request()->billing_country_id,
            'billing_region' => request()->billing_region,
            'billing_city' => request()->billing_city,
            'billing_street' => request()->billing_street,
            'billing_zip' => request()->billing_zip,
            'billing_street_number' => (request()->billing_street_number != '')?request()->billing_street_number:0,
            'billing_mobile' => request()->billing_mobile,
            'billing_phone' => request()->billing_mobile,
            'shipping_country_id' => (isset(request()->shipping['country_id']) ? request()->shipping['country_id'] : request()->billing_country_id),
            'shipping_region' => (isset(request()->shipping['region']) ? request()->shipping['region'] : request()->billing_region),
            'shipping_city' => (isset(request()->shipping['city']) ? request()->shipping['city'] : request()->billing_city),
            'shipping_street' => (isset(request()->shipping['street']) ? request()->shipping['street'] : request()->billing_street),
            'shipping_zip'  => (isset(request()->shipping['zip']) ? request()->shipping['zip'] : request()->billing_zip),
            'shipping_street_number' => (isset(request()->shipping['street_number'])) ? request()->shipping['street_number'] : 1,
            'shipping_phone' => (isset(request()->shipping['mobile']) ? request()->shipping['mobile'] : request()->billing_mobile),
            'shipping_mobile' => (isset(request()->shipping['mobile']) ? request()->shipping['mobile'] : request()->billing_mobile),
            'shipping_name' => (isset(request()->shipping['name']) ? request()->shipping['name'] : request()->billing_lastname),
            'shipping_lastname' => (isset(request()->shipping['lastname']) ? request()->shipping['lastname'] : request()->billing_lastname),
        );

        $cartProducts = [];


        foreach (\Cart::getContent() as $cartProduct) {
            // if product is dynamic
            $associated = Product::find($cartProduct->id)->stock()->first();
            $order['products'][] = [
                'product_id' => $associated->product->id,
                'stock_id' => $associated->id,
                'name' => $cartProduct->name,
                'sku' => $associated->sku,
                'price' => $cartProduct->price,
                'quantity' => $cartProduct->quantity,
            ];
        }

        $order['user'] = [
            // 'order_id'
            // 'shipping_address_id'
            // 'billing_address_id'
            'name' => request()->name,
            'lastname' => request()->lastname,
            'email' => request()->email,
            'mobile' => request()->billing_mobile,
        ];

        $order['address'] = request()->address;
        $order['shipping'] = request()->shipping;
        /*
         * create user and add create Order
         */
        // return $order;

         $order = $this->checkAuthAndCreateOrder($order);
//        return $payment->execute($order);



//        \Cart::clear();

//        return $request->payment_method;
        if($request->payment_method == 'ethniki'){
            return $this->nbgPayment($cartDetails['total'], date('d-m-Y s:i'), 'Some referense');
        }


        return redirect('/cart/checkout/complete')->with('order_id', $order->id);

//        return view(env('THEME') .'.success_checkout', compact('order'));
    }

    public function payments(Request $payment)
    {
        $paymentSelected = Payment::find($payment->get('payment_id'));

        /**
         * find and return the path of module
         */
        $paymentType = $this->PaymentClass($paymentSelected);

        $getModule = \Module::find($paymentType);
        $aName = (string) $getModule->getName();
        $payment = "\Modules\\$aName\Entities\\$aName";

        $payment = new $payment;
        return $payment->info();
    }

    /**
     * find payment class
     * @param $paymentSelected
     * @return string
     */
    protected function PaymentClass($paymentSelected)
    {
        $paymentType = \Module::find($paymentSelected->class);
        //        $paymentType = "App\\Extensions\\" . $paymentSelected->class;

        return $paymentType;
    }


    //@todo delete method
    protected function checkPayment($request)
    {
        /**
         * get payment
         */
        $paymentSelected = Payment::find($request->payment_id);

        /**
         * find and return the path of module
         */
        $paymentType = $this->PaymentClass($paymentSelected);

        $getModule = \Module::find($paymentType);
        ($aName = (string) $getModule->getName());
        $payment = "\Modules\\$aName\Entities\\$aName";

        return $payment = new $payment;
    }

    /**
     * @param CheckoutRequest $request
     */
    protected function checkAuthAndCreateOrder($request)
    {
        /**
         * if user is signed in
         */
        if (Auth::check()) {
            // return 'user loged in ';
            $user = Auth::user();
        } elseif (request()->createAccount == 1) {
            $validatedData = request()->validate([
                'password' => 'required|confirmed',
                'email' => 'required|email',
            ]);

            // $user = User::where('email', $request['user']['email'])->first();
            // $user->password = Hash::make(request()->password);
            // $user->subscribed = 1;
            // $user->save();
            // return $user;

            DB::beginTransaction();

            try {
                $user = User::where('email', $request['user']['email'])->first();

                if (!$user) {
                    $user = User::create($request['user']);
                    $user->password = Hash::make(request()->password);
                    // $user->subscribed = 1;
                    $user->save();
                    $user->address()->create($request['address']);
                // $user = User::create(['email' => request()->email, 'password' => Hash::make(request()->password)]);
                    // $user = User::create($request['user']);
                    // $user->address()->create($request['address']);
                } elseif ($user && $user->subscribed == 0) {
                    $user->password = Hash::make(request()->password);
                    $user->subscribed = 1;
                    $user->save();
                    $user->address()->create($request['address']);
                    DB::commit();
                }

                // $customer = $user->customer()->create(request()->all());
                // $customer->address()->create($request->address);
            } catch (ValidationException $e) {
                // DB::rollback();
                throw $e;
            }

            // $order = (new OrderController)->store($request);
        } else {
            $user = User::where('email', $request['user']['email'])->first();
            if (!$user) {
                $user = User::create($request['user']);
                $user->address()->create($request['address']);
            }
        }


        $order = $user->orders()->create($request['order']);
//        $request['details']['user_id'] = $user->id;
        $detail = $order->details()->create($request['details']);

        foreach($request['products'] as $key => $rp){
            $request['products'][$key]['order_id'] = $order->id;
        }

        $detail->products()->createMany($request['products']);
        foreach (\Cart::getConditions() as $condition) {
            $voucher = Voucher::where('code', $condition->getName())->increment('uses');
            $detail->vouchers()->attach($voucher);
        }

        return $order;
    }

    /*
     * Products in Cart
     */
    public function cartItems()
    {
        $items = [];

        \Cart::getContent()->each(function ($item) use (&$items) {
            $items[] = $item;
        });
        return $items;
    }

    /*
     * Cart Details
     * total
     *
     */
    public function cartDetails()
    {
        return [
            'total_quantity' => \Cart::getTotalQuantity(),
            'sub_total' => \Cart::getSubTotal(),
            'total' => \Cart::getTotal(),
        ];
    }

    /*
     * add product in cart
     */
    public function add(Request $request)
    {

        //clear previews cart items
//        \Cart::clearCartConditions();

        switch ($request->get('type')){
            case 'simple':
               return $this->addSimple($request);
                break;
            case 'configurable':
               return $this->addConfigurable($request);
                break;
            case 'dynamic':
            return   $this->addDynamic($request);
                break;
        }





        $associated = Associated::where('id', request('associated'))->first();

        if (is_null($associated)) {
            abort(404); //@todo return custom error
        }

        $product = $associated->product;
        $saleCondition = null;

        /**
         * Assign products to cart
         */
        $product = [
            'id' => $associated->id,
            'name' => $product->title,
            'price' => $product->price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $associated->product->id,
            'attributes' => [
                'associated_id' => $associated->id,
                'size' => $associated->value->title,
                // 'color' => $associated->product->color->title,
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        return redirect()->route('cart');
    }

    public function addConfigurable($request){

         $stock = Stock::findOrFail($request->get('stock'));
         $product = $stock->product;

        $price = $product->price;
        $product = [
            'id' => $product->id,
            'name' => $product->title,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'img_url' => $product->getFirstMediaUrl(),
            ],
            // 'conditions' => $saleCondition,
        ];



        \Cart::add($product);
        // show modal for cart redirect
        $request->session()->flash('modal', true);
        return redirect()->back();
    }


    public function addSimple($request){
        $product = Product::where('id', $request->get('product'))->first();
        $price = $product->price;
        $product = [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];


        \Cart::add($product);
        return redirect()->route('cart');
    }
    public function dynamic_price(Request $request){

        $product = \App\Product::find($request->get('product'));
        $width = $request->get('width');
        $depth = $request->get('depth');

        $fields = [
            'width' => $request->get('width'),
            'depth' => $request->get('depth')
        ];

         $price = $product->price;
        $query = json_decode($product->calculator->query, true);

        for($a = 0; $a < count($query['children']); $a++){
            echo '-'.$a . "\n";
            $item = $query['children'][$a]['query']['children'];

            echo '-- '.$item[0]['query']['rule'] . "\n";
            if(isset($fields[$item[0]['query']['rule']])){

                if($fields[$item[0]['query']['rule']] >= $item[0]['query']['value']){
                    echo $item[1]['query']['value']. "\n";
                    if($fields[$item[0]['query']['rule']] <= $item[1]['query']['value']  )
                        continue;

//                    echo $item[2]['query']['value']. ' ';
                     $price += $item[2]['query']['value'];
//                     echo $price;
//                    echo 'sdf '. $item[2]['query']['value'];
                }
            }
        }
        return $price ;
    }


    public function addDynamic( $request)
    {
        \Cart::clearCartConditions();



        $product = Product::where('id', $request->get('product'))->first();
        if (is_null($product)) {
            abort(404); //@todo return custom error
        }


        $saleCondition = null;

        $width = $request->get('width');
        $depth = $request->get('depth');

        $fields = [
            'width' => $request->get('width'),
            'depth' => $request->get('depth')
        ];

         $price = $product->price;
        $query = json_decode($product->calculator->query, true);


        for($a = 0; $a < count($query['children']); $a++){
             $item = $query['children'][$a]['query']['children'];
            if(isset($fields[$item[0]['query']['rule']])){

                if($fields[$item[0]['query']['rule']] >= $item[0]['query']['value']  && $fields[$item[0]['query']['rule']] <= $item[1]['query']['value']   ){
                    $price += $item[2]['query']['value'];
                }
            }
        }


        /**
         * Assign products to cart
         */
        $product = [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'width' => $width,
                'size' => $depth,
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];


        \Cart::add($product);
        return redirect()->route('cart');
    }

    public function voucher_remove($name)
    {
        \Cart::removeCartCondition($name);
        return back();
    }

    public function add_voucher(Request $request)
    {
        \Cart::clearCartConditions();
        $productWithSpecialPriceAmmound = 0;
        $productWithDiscounts = false;
        $checkCartTotal = false;
        $productWithSpecialPrice = [];
        $VoucherAConditionChecker = [];
//        $VoucherAConditionChecker['totalCart'] = false;
        $request->validate(['code:required']);
        $voucher = Voucher::where('code', $request->code)->first();


        if (!$voucher) {
            $request->session()->flash('message', 'Δέν βρέθηκε ο εκπτωτικός κωδικός!');
            return back();
        }
        if($voucher->expires_at && $voucher->expires_at <= Carbon::now()){
            $VoucherAConditionChecker['voucher_expired'] = false;
            $request->session()->flash('message', 'O εκπτωτικός κωδικός έχει λύξει!');
            return back();
        }

        if($voucher->query){
            $query = json_decode($voucher->query, true);
             $cartProducts = \Cart::getContent();
            foreach ($cartProducts as $p) {
                \Cart::clearItemConditions($p['id']);

                $product = Associated::find($p->id)->product;
                if($product->special_price > 0 || $product->rule_id > 0){
                    $productWithSpecialPrice[$product->id] = true;
                }else{

                    $productWithSpecialPriceAmmound += $product->price;
                    $productWithSpecialPrice[$product->id] = false;
                }

                foreach ($query as $q) {

                    switch ($q['type']) {
                        // check if voucher can be applied in discount products

                        case 'season':
                        case 'brand':

                            $type = $q['type'];
                            if($q['query'] == '='){
                                if(in_array($product->$type->attribute_field, $q['value'])){
                                    $VoucherAConditionChecker[$q['type']] = true;
                                }else{
                                    $VoucherAConditionChecker[$q['type']] = false;
                                    $request->session()->flash('message', 'Το κουπόνι δεν ισχύει για προϊόντα με αυτό το χαρακτηριστικό!');
                                }
                            }else{
                                if(!in_array($product->$type->attribute_field, $q['value'])){
                                    $VoucherAConditionChecker[$q['type']] = true;
                                }else{
                                    $VoucherAConditionChecker[$q['type']] = false;
                                    $request->session()->flash('message', 'Το κουπόνι δεν ισχύει για προϊόντα με αυτό το χαρακτηριστικό!');
                                }
                            }

                            break;
                        case 'dicounts':
                            if($q['value'] == "false"){
                                $productWithDiscounts = false;
                                if( $product->special_price > 0 || $product->rule_id > 0){
                                    $VoucherAConditionChecker['no_discount_price'] = false;
                                    $request->session()->flash('message', 'Το κουπόνι δεν ισχύει για προϊόντα με έκπτωση!');
                                } else {
                                    $productWithDiscounts = true;
                                    $VoucherAConditionChecker['no_discount_price'] = true;
                                }
                            }else{
                                $VoucherAConditionChecker['no_discount_price'] = true;
                            }
                            break;

                        case 'cart_total':

                            $cartTotal = $q['value'];
                            $checkCartTotal = true;
                             if (\Cart::getSubTotal() > $q['value']) {
                                 $VoucherAConditionChecker['totalCart'] = true;
                             }else{
                                 $VoucherAConditionChecker['totalCart'] = false;
                                 $request->session()->flash('message', 'Το σύνολο του καλαθιού είναι μικρότερο απο το απαιτούμενο!');
                             }
                            break;
                    }
                }

                 $voucherPassQueryChecker = !in_array(false, $VoucherAConditionChecker, true);
                if(!isset($VoucherAConditionChecker['totalCart']) || $VoucherAConditionChecker['totalCart'] == false){
                    if($voucherPassQueryChecker){
                        $productID = $p['id'];
                        $value = ($voucher->is_fixed == 1) ?  "-" .$voucher->discount_amount  :  "-" .$voucher->discount_amount . '%';
                        $coupon101 = new \Darryldecode\Cart\CartCondition(array(
                            'name' => $voucher->name,
                            'type' => 'coupon',
                            'value' => $value,
                        ));
                        \Cart::addItemCondition($productID, $coupon101);
                        $request->session()->flash('flash_message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                    }
                }
            }

            if(isset($VoucherAConditionChecker['totalCart']) && $VoucherAConditionChecker['totalCart']  && \Cart::getSubTotal() > $cartTotal  ){

                if(count(\Cart::getContent()) == 1 ){
                    if($productWithDiscounts ){
                        $this->applyVoucher($voucher);
                        $request->session()->flash('message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                        return back();
                    }else{

                        $request->session()->flash('message', 'Το κουπόνι δεν ισχύει για προϊόντα με έκπτωση!');
                        return back();
                    }
                }elseif(count(\Cart::getContent()) > 1){
                    if($productWithDiscounts){
                        $this->applyVoucher($voucher);
                        $request->session()->flash('message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                        return back();
                    }elseif($productWithSpecialPriceAmmound >= \Cart::getSubTotal()){
                        $this->applyVoucher($voucher);
                        $request->session()->flash('message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                        return back();
                    }else{
                        $discountedProduct = [];

                        foreach ($cartProducts as $p) {
                            $product = Associated::find($p->id)->product;

                            if($voucher->is_fixed != 1  && $product->special_price == 0 || $product->special_price == null && $product->rule_id == 0.0 || $product->rule_id == null){
                                $discountedProduct[] = true;
                                $setVoucher = true;
                                $productID = $p['id'];
                                $value = ($voucher->is_fixed == 1) ?  "-" .$voucher->discount_amount  :  "-" .$voucher->discount_amount . '%';
                                $coupon101 = new \Darryldecode\Cart\CartCondition(array(
                                    'name' => $voucher->name,
                                    'type' => 'coupon',
                                    'value' => $value,
                                ));
                                \Cart::addItemCondition($productID, $coupon101);
                            }else{
                                $discountedProduct[] = false;
                            }
                        }
                        $getVoucher = !in_array(false, $discountedProduct, true);
                        if($getVoucher){
                            $request->session()->flash('message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                            return back();
                        }else{
                            $request->session()->flash('message', 'Δέν έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                            return back();
                        }
                    }
                }
            }else{
//                $request->session()->flash('message', 'Το σύνολο του καλαθιού είναι μικρότερο απο το απαιτούμενο!');
                return back();
            }
        }else {
            $this->applyVoucher($voucher);
            $request->session()->flash('flash_message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
            return back();
        }



        return back();
    }

    public function applyVoucher(Voucher $voucher)
    {
        $condition = new \Darryldecode\Cart\CartCondition([
            'name' => $voucher->code,
            'type' => 'ΚΟΥΠΟΝΙ', // sale , promo , misk ,
            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => ($voucher->is_fixed == 1) ?  "-" .$voucher->discount_amount :  "-" .$voucher->discount_amount . '%',
            'attributes' => [ // attributes field is optional
                'description' => $voucher->description,
            ]
        ]);
        \Cart::condition($condition);
    }

    /*
     * delete product in cart
     */
    public function delete($id)
    {
        \Cart::clearCartConditions();
        \Cart::remove($id);
        if (request()->ajax()) {
            return 'success';
        }
        return back();
        return response([
            'success' => true,
            'data' => $id,
            'message' => "cart item {$id} removed.",
        ], 200, []);
    }
}
