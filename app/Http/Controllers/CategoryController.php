<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        return view('manage.categories.index');
    }
    public function listJson(Request $request){
        return $products = Category::paginate($request->get('paginate'));
    }

    public function categoriesTree(){
        return Category::with('children', 'children.children')
            ->where('parent_id', 0)
            ->orderBy('order')
            ->get();
    }

    public function update(Category $category, Request $request){
      return  $category->update($request->input());
    }
    public function create(Request $request){
        Category::create($request->all());
    }
    public function category(Category $category){
        return $category;
    }

    public function get_categories_for_query(){
        return Category::with('children')
            ->where('parent_id', 0)
            ->orderBy('order')
            ->get();
    }
}
