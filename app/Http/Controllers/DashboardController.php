<?php

namespace App\Http\Controllers;

use App\Field;
use App\Order;
use App\OrderProduct;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
class DashboardController extends Controller
{


    public function __construct()
    {
        if (\Auth::guest()) {
            //is a guest so redirect
            $view = env('THEME') .'.auth.login';

            if(!view()->exists($view)){
                $view = 'default.auth.login' ;
            }
            return view($view);
        }

    }


    public function dashboardSearch(Request $request){


        $results = '<ul>';

        $products = Product::where('sku', 'like', '%' . $request->get('q') . '%')
            ->orWhere('title', 'like', '%' . $request->get('q') . '%')
            ->get();

        if(count($products) > 0){
            foreach ($products as $p){
                $results .= "<li> <a href='/manage/products/$p->id'> $p->title  </a></li>";
            }
        }



        $results .= '<ul>';


        return $results;
    }


    public function index(){
        $lastOrders  = [];
        $orders = [];
        $date = new Carbon;
        $today_start = Carbon::today();
        $today_end = Carbon::today()->addRealMinutes(1439);
        $getDates = 12 ;
        $date->subWeek(); // or $date->subDays(7),  2014-03-27 13:58:25
        $lastOrders = Order::with('details')->orderBy('id', 'desc')->take($getDates)->get();
//        $allOrders = Order::orderBy('id', 'desc')->take($getDates)->get();
        $i = 0;
//return Carbon::today()->subDays(10)->toDateTimeString();
//         $allOrders->where('created_at', '>=', Carbon::today()->subDays(12)->toDateTimeString());
        while ($i < $getDates) {

            $ordersForThisDay = Order::whereDate('created_at', '=', Carbon::now()->subDays($i))->get()
//                ->where('created_at', '<=', Carbon::today()->subDays($i)->addRealMinutes(1439)->toDateTimeString())
            ;
            $orders['dates'][] = Carbon::today()->subDays($i)->toDateTimeString();
            $orders['orders'][] = $ordersForThisDay->count();
            $orders['total'][] = number_format(
                $ordersForThisDay
                    ->sum('details.total')
                , 2, '.', '');
            $i++;
        };
//        return $orders;

        //stats
        $fields = Field::where(['is_enabled' => 0, 'position'=> 'content'])->get();

        $orderProducts = OrderProduct::where('created_at', '>', $today_start->subDays(7))->with('product')->get();
        // get all values in each type
        $type = [];
        foreach($orderProducts as $op){

            foreach($fields as $field){
                $type['fullDate'][Carbon::parse($op->created_at)->format('d F Y')] = Carbon::parse($op->created_at)->format('d F Y');
                $type['month'][Carbon::parse($op->created_at)->format('F')] = Carbon::parse($op->created_at)->format('F');
                $type['types'][$field->title] = $field->title;
                $type['type'][$field->title][] = $op->product[$field['name']]['value']['title'];
            }
        }

//         count items in each statch
        $stats = [];
        if(count($orderProducts) > 0){
            foreach($fields as $field){
//                $stats[$field->title][] = array_count_values($type['type'][$field->title]);
            }
        }

//        return $lastOrders;
        return view('manage.dasboard' , compact('orders', 'lastOrders', 'stats'));

    }

    public function query(Request $request){

        $q = $request->get('q');
        $products = Product::where('sku', $q);
    }
}
