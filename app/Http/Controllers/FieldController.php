<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Field;
class FieldController extends Controller
{

    public function index(){
        return view('fields.index');
    }

    public function get_fields(){
        return Field::where('is_enabled', 1)->orderBy('positions', 'asc')->get();
    }

    public function add_field_attribute(Request $request){

         $field = $request->get('field');
         $value = $request->get('value');
         $model = $field['field']['model_type'];
        return $newInput = $model::create(['created_at'=> now() , 'title' => $value, 'name'=>str_slug($value, '-').'-'.time()]);
    }

    public function get_field_values(Request $request){


        $field = Field::where('name', $request->get('field'))->first();
        $fieldName = $field->name;
        if($field->type == 'text' ){
            return Product::find($request->get('product'))->$fieldName->first()->get(['id', 'title']);
        }
        elseif($field->type == 'textarea'){
            $fieldName = $field->name;
            return Product::find($request->get('product'))->$fieldName->first()->get(['id', 'body']);
        }

        $model = 'App\Fields\Field'.ucfirst($field->name);
        return $model::get(['id', 'title']);
    }

    public function get_field_values_all(Request $request){


        $field = Field::where('name', $request->get('field'))->first();

        if($field->type == 'text'){
            $fieldName = $field->name;
            return Product::find($request->get('product'))->$fieldName->get(['id', 'body']);
        }

        $model = 'App\Fields\Field'.ucfirst($field->name);
        return $model::get(['id', 'title']);
    }
}
