<?php

namespace App\Http\Controllers;

use App\Product;
use App\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        if (Slider::find(1)) {
//            $slider = Slider::find(1)->with('images')->get();
//        } else {
//            $slider = [];
//        }
        $view = env('THEME') . '.index';
        if(!view()->exists($view)){
            $view = 'default/index' ;
        }


          $featureProducts = Product::where('is_featured', 1)->take(8)->get();

        return view($view, compact('featureProducts'));
    }
}
