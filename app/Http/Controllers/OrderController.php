<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index()
    {

        $data_url = '/manage/orders_list_json';
        $edit_url = 'orders';
        $headers = [
            ['text'=> 'Id', 'value'=> 'order_num'],
            ['text'=> 'Email', 'value'=> 'user.email'],
            ['text'=> 'Τηλ', 'value'=> 'user.mobile'],
            ['text'=> 'Σύνολο', 'value'=> 'details.total'],
            ['text'=> 'Ημ/νια', 'value'=> 'created_at'],
            ['text'=> 'Κατάσταση', 'value'=> 'details.status'],
            ['text'=> 'Επιλογές', 'value'=> 'actions']
            ];

        return view('manage.orders.index', compact('headers', 'edit_url','data_url'));
    }
    public function listJson(Request $request){
        return $products = Order::with('details', 'user')->orderBy('created_at', 'desc')->paginate($request->get('paginate'));
    }

    public function edit($order){
          $order = Order::with('details', 'user', 'products')->find($order);
        return view('manage.orders.edit', compact('order'));
    }

}
