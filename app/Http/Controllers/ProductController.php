<?php

namespace App\Http\Controllers;

use App\Calculator;
use App\Category;
use App\Field;
use App\Fields\FieldSize;
use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use Intervention\Image\Size;
use Elasticsearch\ClientBuilder;
use Algolia\AlgoliaSearch\SearchClient;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductController extends Controller
{

    public function search(Request $request){

        return Stock::with('product')->where('sku', 'like', "%{$request->get('search')}%")->get();
    }
    function calcResult($query){
    }
    function calcGroupToChild($group){

    }

    function calcToQuery($input){
        if(isset($input['children'])){
            foreach($input['children'] as $child){
                if($child['type'] == 'query-builder-rule'){
                    $this->calcToQuery($child);
                }elseif($child['type'] == 'query-builder-group'){
                    $this->calcGroupToChild();
                }
            }
        }

    }


    public function index(){



        //search query
//        return Product::whereHas('Brand', function ( $query) { $query->where('title', 'brand1');})->get();

        return view('manage.products.index');
    }
    public function listJson(Request $request){
         $products = Product::
            when($request->has('search') && $request->get('search') != '' , function ($query) use ($request) {
                $query->where('sku', 'like', "%{$request->get('search')}%")
                ->orWhere('title', 'like', "%{$request->get('search')}%")
                ;
            })
            ->orderBy('id', 'desc')->paginate($request->get('paginate'));
        return $products;
    }

    public function create(){
        return view('/manage.products.create');
    }

    public function show(){
    }

    public function edit(Product $product){
        $product = Product::with( Field::where('position', '!=', 1)->pluck('name')->toArray())->find($product->id);

        $setStock = [];
        $calculator =  ($product->calculator)? $product->calculator->query: json_encode([]);
        $productCategories = $product->categories()->pluck('id');
        return view('/manage.products.edit', compact('product', 'productCategories','calculator'));
    }

    public  function update(Product $product, Request $request){
            $product->update($request->all());
            return $product;
    }


    public function updateCategories(Product $product, Request $request)
    {
        $product->categories()->sync($request->input());
    }

    public function calculatorUpdate(Product $product, Request $request){

        $query = json_encode($request->input());
        if(!$product->calculator){
            $product->calculator()->create(['query' => $query]);
        }else{
            $product->calculator()->update(['query' => $query]);
        }
        return $product->calculator;

    }

    public function fieldsUpdate(Product $product, Request $request){

//add cron to remove unused field text values
        foreach($request->input() as $r){
            if(!isset($r['value'])){
                continue;
            }
            if($r['type'] == 'text' || $r['type'] == 'textarea'){
                $model = $r['model_type'];

                if(isset($r['oldvalue'])){
                   return  $model::find((int)$r['oldvalue'])->update(['body' => $r['value'], 'name'=>str_slug($r['value'].'-'.time(), '-')]);
//                    $newInput = $model::update(['updated_at'=> now() , 'title' => $r['value'], 'name'=>str_slug($r['value'], '-')]);
                }elseif(isset($r['value']) && is_array($r['value'])){

//                    return  $model::find((int)$r['value'][0])->update(['title' => $r['value'], 'name'=>str_slug($r['value'][0].'-'.time(), '-')]);
//                    $newInput = $model::update(['updated_at'=> now() , 'title' => $r['value'], 'name'=>str_slug($r['value'], '-')]);
                }else{

                    $newInput = $model::create(['created_at'=> now() , 'title' => $r['value'], 'name'=>str_slug($r['value'], '-').'-'.time()]);
                    $field = $r['name'];
                 return    $product->$field()->sync($newInput->id);
                }
                // search for value if exist update else create


            }else{
                $field = $r['name'];
                $product->$field()->sync($r['value']);
            }

        }
    }

    public function stockUpdate(Product $product, Request $request){
        $stock = $request->all();
        for($i=0; $i<count($stock);$i++){

            $product->stock()->updateOrCreate(
                ['sku' => (isset($stock[$i]['sku']) ? $stock[$i]['sku'] : null), 'product_id'=> $product->id],
                [
                    'sku'=>$stock[$i]['sku'],
                    'ean'=> (isset($stock[$i]['ean']))?$stock[$i]['ean']:null,
                    'barcode'=> (isset($stock[$i]['barcode']))?$stock[$i]['barcode']:null,
                    'price'=> (isset($stock[$i]['price']))?$stock[$i]['price']:null,
                    'quantity'=>(integer)$stock[$i]['quantity']
                ]
            );
        }

        return $product->stock;
    }

    public function get_stock(Product $product){
        foreach($product->stock as $key => $stock){
            if(isset($stock['variations'])){
                foreach($stock['variations'] as $vkey => $variation) {
                     $field = Field::where('id', $variation['field_id'])->first();
                    $product->stock[$key]['variations'][$vkey]['field'] = $field;
                    $model =  $variation['model_type'];
                   if(isset($product->stock[$key]['variations'][$vkey]['field'])){
                       $product->stock[$key]['variations'][$vkey]['field']['values'] = $model::all();;
                   }
                }
            }
        }
        return $product->stock;
    }

    public function getMedia(Product $product){

        $media = $product->getMedia();
        foreach ($media as $m){
            $m['path'] = $m->getUrl('product');
        }
        return $media;
    }

    public function addMedia(Product $product, Request $request)
    {
       $image = $request->all();

         $image = $image['image'];

//        if (is_a($image, 'Illuminate\Http\UploadedFile')) {
        $product->addMediaFromBase64($image)->usingFileName(str_random().'.png')->toMediaCollection();

//            $product->addMediaFromBase64($image)->toMediaCollection('product');
            // $mediaItems[0]->delete();
            return true;
//        }

        return 'true';
    }
    public function deleteMedia($id){

        Media::find($id)->delete();
        return true;

    }



    public function store(Request $request){

        $client = SearchClient::create(
            'QOXUJDLYXT',
            'dc0800d5e68023cda7bf62cbb5f59dcf'
        );
        $index = $client->initIndex('products');

        $product = new Product;
        $product->title = $request->get('title');
        $product->price = 0.00;
        $product->group = 0;
        $product->name = $request->get('name');
        $product->sku = $request->get('sku');
        $product->product_type = $request->get('product_type');
        $product->is_active = $request->get('is_active');
        $product->tax_class_id = 1;
        $product->save();


        $index->saveObject(
            [
                'objectID' => $product->id,
                'title' => $product->title,
                'title_en' => $product->title,
                'price' => $product->price,
                'sku' => $product->sku,
                'is_active'  => $product->is_active
            ]
        );

        return $product;
        return redirect('/manage/products/'.$product['id']);

    }
    public function destroy(Product $product){
        Product::destroy($product->id);
    }

    public function searchAlgolia(){
        $client = SearchClient::create(
            'QOXUJDLYXT',
            'dc0800d5e68023cda7bf62cbb5f59dcf'
        );
        $index = $client->initIndex('products');

        return   $objects = $index->search('Ανδ');

    }

}
