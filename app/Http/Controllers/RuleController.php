<?php

namespace App\Http\Controllers;

use App\Product;
use App\Rule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RuleController extends Controller
{
    var $groupOr = false;
    public function index()
    {
        $edit_url = '/manage/rules';
        $data_url = '/manage/rules_list_json';
        $headers = [
                [
                    'text'=> 'Title',
                    'align'=> 'start',
                    'sortable'=> true,
                    'value'=> 'title',
                ],
            ['text'=> 'Ποσοστό Έκπτωσης', 'value'=> 'discount_value'],
            ['text'=> 'Ενεργό', 'value'=> 'is_active'],
            ['text'=> 'Επιλογές', 'value'=> 'actions']

        ];

        return view('manage.rules.index', compact('headers', 'data_url', 'edit_url'));
    }
    public function listJson(Request $request){
        return $products = Rule::paginate($request->get('paginate'));
    }



    public function queryls($rules, $results)
    {
        foreach ($rules as $rule) {
            switch ($rule['type']) {
                case 'category':
                    $results = $results->when($rule['type'] == 'category', function ($query) use ($rule) {
                        return $query->byCategory($rule['value']);
                    });
                    break;
            }
        }

    }

    /*
     * used in rule
     * to search products
     */
    public function filter_products(Request $request){

        return $this->query($request->get('query'));
    }

    public function query($input){

        $products = Product::query();

        $globalLogicalOperator = $input['logicalOperator'];
        $this->groupOr = ($globalLogicalOperator == 'any')? true:false;
        foreach($input['children'] as $q){
            if($q['type'] == 'query-builder-rule'){

                $products = $this->querymanager($q, $products );
            }elseif($q['type'] == 'query-builder-group'){
                $this->groupOr = false;
                $this->calcGroupToChild($q, $products);
            }
        }

        return $products->get(['id', 'sku', 'title', 'price', 'special_price','rule_id']);
    }

    function calcGroupToChild($group, $products){

        foreach($group['query']['children'] as $q){

            if($q['type'] == 'query-builder-rule'){
                $products = $this->querymanager($q, $products);
            }elseif($q['type'] == 'query-builder-group'){
                $this->calcGroupToChild($q, $products);
            }
            $this->groupOr = true;
        }
    }

    function querymanager($query, $products){

        switch ($query['query']['rule']){
            case 'category':
                $productincategories = DB::table('product_category')->whereIn('category_id', $query['query']['value'])->groupBy('product_id')->pluck('product_id');
                return  $products->whereIn('id', $productincategories);
                break;
            case 'sku':
                return $products->where('sku',  $query['query']['operator'], $query['query']['value']);
                break;
            case 'price':

                return $products->where('price', $query['query']['operator'] ,$query['query']['value']);
                break;

            case 'brand':
                if($this->groupOr){
                    $field = $query['query'];

                    return $products->orWhereHas('brand', function($q) use ($field){
                        $q->where('id', $field['operator'],$field['value']);
                    });
                }else{
                    $field = $query['query'];
                    return $products->whereHas('brand', function($q) use ($field){
                        $q->where('id', $field['operator'],$field['value']);
                    });
                }
                break;

            case 'season':
                $field = $query['query'];
                if($this->groupOr){

                    return $products->orWhereHas('season', function($q) use ($field){
                        $q->where('id', $field['operator'], $field['value']);
                    });
                }else{
                    return $products->whereHas('season', function($q) use ($field){
                        $q->where('id', $field['operator'], $field['value']);
                    });
                }

                break;
        }

        return $products;
    }



    function calcToQuery($input){
        if(isset($input['children'])){
            foreach($input['children'] as $child){
                if($child['type'] == 'query-builder-rule'){
                    $this->calcToQuery($child);
                }elseif($child['type'] == 'query-builder-group'){
                    $this->calcGroupToChild();
                }
            }
        }

    }


    public function store(Request $request){


        $rule = new Rule();
        $rule->title = $request->title;
        $rule->is_active = $request->is_active;
        $rule->discount_type = $request->discount_type;
        $rule->query = $request->get('query');
        $rule->discount_value = $request->discount_value;
        $rule->save();


        $products = $this->query($request->get('query'));
        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => $rule->id]);

        return $rule;
    }

    public function show(Rule $rule){

        return view('manage.rules.edit', compact('rule'));
    }

    public function update(Rule $rule, Request $request){
        $rule->update($request->input());
        $products = $this->query($request->get('query'));
        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => null]);
        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => $rule->id]);
    }

    public function create(){
        return view('manage.rules.create');
    }

    public function destroy($id)
    {
        $rule = Rule::findOrFail($id);
        $products = $this->query($rule->get('query'));
        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => null]);
        Rule::destroy($id);
        return $this->index();
    }


}
