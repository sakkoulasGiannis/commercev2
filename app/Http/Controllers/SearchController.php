<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request = null)
    {
        $paginate = 200;

        // set pagination

        if (request()->has('q')) {
//            $products = Product::hasInventory();
            $products = Product::query();

            $products = $products
                ->hasStock()
                ->where('is_active', 1)
                ->where('products.sku', 'LIKE', '%' . request()->get('q') . '%')
                ->orWhere('name', 'LIKE', '%' . request()->get('q') . '%')
                ->orWhere('description', 'LIKE', '%' . request()->get('q') . '%')
                ->get()
            ;


            return view(setting('theme') . '.search', compact('products'));
        }
        return back();
    }

}
