<?php

namespace App\Http\Controllers;

use App\Field;
use App\Category;
use App\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
class SegmentController extends Controller
{


    public $product;
    public $category;
    public $currentCategory;
    public $path;
    public $tree;
    public $path_N;

//    public function returnRequest($slug, $parent = null)
//    {
//        $category = Category::where('name', $slug)->first();
//        if ($category) {
//            return $this->renderCategoryPage($category);
//        }
//        $product = Product::where('name', $slug)->first();
//        if ($product) {
//            $this->product = $product;
//            return $this->renderProductPage($product);
//        }
//
//        return App::abort(404);
//    }

    public function category($categorySlug, $ProductOrCategory, $parent = null)
    {
        $parentCategory = Category::where('name', $categorySlug)->firstOrFail();
        $category = Category::where([['name', '=', $ProductOrCategory], ['parent_id', '=', $parentCategory->id]])->first();
        if ($category) {
            $parentCategory = Category::where('name', $categorySlug)->first();

            if ($category->parent->slug === $categorySlug) {
                if (!is_null($parent)) {
                    return $this->checkForProduct($ProductOrCategory, $parent);
                } else {
                    return $this->renderCategoryPage($category);
                }
            } else {
                return App::abort(404);
            }
        }
        return $this->checkForProduct($categorySlug, $ProductOrCategory);
    }

//    public function subCategory($categorySlug, $subCategorySlug, $ProductOrCategory)
//    {
//        $category = Category::where('name', $subCategorySlug)->first();
//        if ($category) {
//            return $this->category($categorySlug, $subCategorySlug, $ProductOrCategory);
//        }
//    }

    public function checkForProduct($categorySlug, $ProductOrCategory)
    {
        $product = Product::where('name', $ProductOrCategory)->first();

        if ($product) {
            if ($product->categories()->where('name', $categorySlug)->firstOrFail()) {
                return $this->renderProductPage($product);
            } else {
                return App::abort(404);
            }
        }

        return App::abort(404);
    }

    /**
     * render product page
     */
    public function renderProductPage($product)
    {
        if(Cache::get('product_'.$product->slug)){
            return Cache::get('product_'.$product->slug);
        }
        $apothiki = $product->stock;
        $block = null;
        $fields = Field::get();
        return view(env('THEME') . '.product', compact('product', 'apothiki', 'block', 'fields'));
    }


    /**
     * render the category with products
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderCategoryPage(Category $category, Request $request = null)
    {

        $paginate = 24;
        $pagination = [24 => 24, 48 => 48, 72 => 72, 96 => 96];
        // set pagination
        if (request()->has('pagination')) {
            session()->put('paginate', request()->get('pagination'));
        } elseif (!session()->exists('paginate')) {
            session()->put('paginate', $paginate);
        }


        $page = request()->has('page') ? request()->get('page') : 1;
//        $cache = (isset($_GET['cache']))? false : false;
//        if($cache && Cache::get('category_'.$category->id.'_page_'.$page .\Request::fullUrl().'_pagination_'.session()->get('paginate'))){
//            return Cache::get('category_'.$category->id.'_page_'.$page.\Request::fullUrl().'_pagination_'.session()->get('paginate'));
//        }


        $path = request()->path();
        $path = explode('/', $path);

        $setCat = null;

        $products = $category->products()->hasStock();

        $fields = Field::where('is_enabled', 1)->get();

        // build fields array
        foreach ($fields as $val => $f) {

            $searchProducts =  $products->with($f->name)->get()->pluck($f->name);
            $fieldValues = $searchProducts->flatten()->unique('id');
             $fields[$val]['values'] = collect($fieldValues);
        }

        // filter products
        if (request()->has('query')) {
            foreach ($fields as $field) {
                if (request()->has($field->name) && request()->get($field->name) != '') {
                    $products = $products->filter($field->name, request()->get($field->name));
                }
            }
            if(request()->has('discounted') && request()->get('discounted') == 'true'){
                $products = $products->where('special_price', "!=", null )->orWhere('rule_id', "!=", null);
            }
        }  //end elseif


        if(request()->has('sort') && request()->get('sort') == 'price_asc' || request()->get('sort') == 'price_desc'){
            if(request()->has('debug') && request()->get('debug') == 'true'){
                $sort = (request()->get('sort') == 'price_asc')? 'ASC': 'DESC';
                $products->leftjoin('rules', 'products.rule_id', '=' ,'rules.id')
                    ->orderByRaw('CASE WHEN products.special_price < products.price AND products.special_price > 1 THEN products.special_price
                     WHEN products.rule_id > 0 THEN products.price - (products.price * (rules.discount_value / 100))
                ELSE
                products.price
                END '.$sort);
            }else{
                $sort = (request()->get('sort') == 'price_asc')? 'ASC': 'DESC';
                $products->leftjoin('rules', 'products.rule_id', '=' ,'rules.id')
                    ->orderByRaw('CASE WHEN products.special_price < products.price AND products.special_price > 1 THEN products.special_price
                    WHEN products.rule_id > 0 THEN products.price - (products.price * (rules.discount_value / 100))
                    ELSE
                    products.price
                    END '. $sort);
            }

        }elseif(request()->has('sort') && request()->get('sort') == 'discount_asc' || request()->get('sort') == 'discount_desc'){
            $sort = (request()->get('sort') == 'discount_asc')? 'ASC': 'DESC';
            $products->leftjoin('rules', 'products.rule_id', '=' ,'rules.id')
                ->orderByRaw('CASE WHEN products.special_price > 2  THEN ((products.price - products.special_price)/products.price) * 100
                    ELSE
                    rules.discount_value
                    END '. $sort);
        }elseif(request()->has('sort') && request()->get('sort') == 'season_asc' || request()->get('sort') == 'season_desc'){
            $sort = (request()->get('sort') == 'season_asc')? 'ASC': 'DESC';
            $products
                ->orderBy('products.created_at',  $sort);
        }

//        $products = $products->paginate(session()->get('paginate'))->appends(Input::except('page'));

        $products = $products->paginate(session()->get('paginate'));


        $expiresAt = now()->addMinutes(30);
//        if($cache){
//            return Cache::remember('category_'.$category->id.'_page_'.$page.\Request::fullUrl().'_pagination_'.session()->get('paginate'),$expiresAt, function () use($products, $fields, $category, $pagination) {
//                return view(setting('theme') . '.category',compact('products', 'fields', 'category', 'pagination'))->render();
//            });
//        }

        return view(env('THEME') . '.category', compact('products', 'fields', 'category', 'pagination'));
    }

    public function search($products, $field)
    {
        $fieldModel = $field->model_type;
        $value = $fieldModel::where('name', request()->get($field->name))->first();
        $products->whereHas($field->name, function ($query) use ($value) {
            $query->where('attribute_field', $value->id);
        });

        return $products;
    }

    public function getCat()
    {

        $path = request()->path();
        $path = explode('/', $path);
        $this->tree = Category::where('parent_id', 0)->get();
        $this->path = $path;
        $this->path_N = 0;

         $tree = $this->tree->where('name', $this->path[$this->path_N])->first();
        return $this->checkCat_rec($tree);
    }

    public function checkCat_rec($category)
    {
        if (count($this->path) > 0 && $this->path_N < count($this->path) - 1 && $category instanceof Category && $category && $category->children) {
            $this->path_N++;
            $category = $category->children->where('name', $this->path[$this->path_N])->first();
            if ($category && $category instanceof Category) {
                $this->currentCategory = $category;
            }
            return $this->checkCat_rec($category);
        } elseif ($category && $category instanceof Category) {
            return $this->renderCategoryPage($category, null);
        } else {
            $product = Product::where('name', $this->path[$this->path_N])->first();
            if ($product) {
                if ($this->currentCategory && !$this->currentCategory->dynamic) {
                    return $this->renderProductPage($product);
                } elseif ($this->currentCategory && $this->currentCategory->products->where('name', $this->path[$this->path_N])) {
                    return $this->renderProductPage($product);
                } else {
                    return $this->renderProductPage($product);
                }
            }

            return App::abort(404);
        }
    }

    public function runQuery($query)
    {

        $products = Product::inStock();
        foreach (json_decode($query, true) as $rule) {
            switch ($rule['type']) {
                case 'category':
                    $products = $products->when($rule['type'] == 'category', function ($query) use ($rule) {
                        return $query->byCategory($rule['value']);
                    });
                    break;
                case 'price':
                    $products = $products->where($rule['type'], $rule['query'], $rule['value']);
                    break;
                case 'inventory':
                    $products = $products->inventory($rule['value'], $rule['query']);
                    break;
                case "created_at":
                    $products = $products->whereDate('products.created_at', '>=', Carbon::now()->subDays($rule['value']));
                    break;
                case 'discounted':
                    $products->where('special_price', '>', 0)
                        ->orWhere('rule_id', '>' , 0);
                    break;
                default:

//                    $products = $products->filter('color', 'mple');
                    $products = $this->filter($products, $rule);
                    break;
            }
        }
        return $products;
    }


    public function filter($products, $data)
    {
        // search by date
        if (isset($data['inputType']) && $data['inputType'] == 'datetime') {
            return $products->where($data['type'], $data['query'], $data['value']);
        }

        if (isset($data['inputType']) && $data['inputType'] == 'sku') {
            return $products->where($data['type'], $data['query'], $data['value']);
        }

        //search by attribute
        $products->whereHas($data['type'], function ($query) use ($data) {

            if (is_array($data['value'])) {
                if ($data['query'] == '=') {
                    $query->whereIn('attribute_field', $data['value']);
                } else {
                    $query->whereNotIn('attribute_field', $data['value']);
                }
            } else {
                $query->where('attribute_field', $data['query'], $data['value']);
            }
        });

        return $products;
    }
}
