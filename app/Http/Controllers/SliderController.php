<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use App\SliderImage;

class SliderController extends Controller
{
    /**
     * @todo remove the show funciton from web
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_url = '/manage/sliders_list_json';
        $headers = [
            [
                'text'=> 'Title',
                'align'=> 'start',
                'sortable'=> true,
                'value'=> 'title',
            ],
        ];

        return view('manage.sliders.index', compact('headers', 'data_url'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slider = new Slider;
        $slider->save();
        return redirect()->route('sliders.show', $slider->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Slider::create($request);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        return view('manage.sliders.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('manage.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $slider->update($request->input());

        return back();
    }

    public function updateImage(SliderImage $image, Request $request)
    {
        $image->update($request->input());
        return back();
    }

    public function getImages(Slider $slider){

        return $slider->images;
    }

    public function addPhoto(Slider $slider, Request $request)
    {

        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $path = $request->image->storeAs("sliders/$slider->id", $imageName, 'public');

        $slider->images()->create(
            [
                'url' => $path,
                'alt' => $request->alt,
                'href' => $request->href,
                'body' => $request->body,
            ]
        );
        return $slider->images();
    }

    public function destroyPhoto(SliderImage $image)
    {
        $image->destroy($image->id);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->destroy();
        return $this->index();
    }
}
