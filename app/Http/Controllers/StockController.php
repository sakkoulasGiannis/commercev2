<?php

namespace App\Http\Controllers;

use App\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function destroy(Request $request){
        Stock::destroy($request->get('id'));
    }
}
