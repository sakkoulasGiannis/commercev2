<?php

namespace App\Http\Controllers;

use App\Field;
use App\Stock;
use App\StockVariation;
use Illuminate\Http\Request;

class StockVariationController extends Controller
{

    public function create(Stock $stock, Request $request ){

        $field = Field::where('name', $request->get('fieldId'))->first();

        StockVariation::create([
            'stock_id'=>$request->get('stockId'),
            'field_id'=>$field->id,
            'model_type' => "\App\Fields\Field".ucFirst($request->get('fieldId')),
            'model_id'=>$request->get('valueId')
        ]);
        return $stock->variations;
    }
    public function destroy(Request $request){
        StockVariation::destroy($request->get('id'));
    }
}
