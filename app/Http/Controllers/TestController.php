<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function test()
    {
        $cateogries = [
            '1,0,1,0,Root Catalog',
            '3,6,1/6/3,2,Γυναικεία',
            '4,3,1/6/3/4,3,Γόβες',
            '5,3,1/6/3/5,3,Πέδιλα',
            '6,1,1/6,1,e-shoes.gr',
            '7,3,1/6/3/7,3,Μπαλαρίνες',
            '8,3,1/6/3/8,3,Casual',
            '9,3,1/6/3/9,3,Σαγιονάρες',
            '10,3,1/6/3/10,3,Πλατφόρμες',
            '12,3,1/6/3/12,3,Μπότες',
            '13,3,1/6/3/13,3,Μποτάκια',
            '14,6,1/6/14,2,Ανδρικά',
            '15,14,1/6/14/15,3,Μποτάκια',
            '16,14,1/6/14/16,3,Μπότες',
            '17,14,1/6/14/17,3,Μοκασίνια - Loafers',
            '18,14,1/6/14/18,3,Σανδάλια',
            '19,14,1/6/14/19,3,Casual',
            '20,14,1/6/14/20,3,Αθλητικά',
            '21,6,1/6/21,2,Παιδικά',
            '22,21,1/6/21/22,3,Μπότες - Μποτάκια',
            '23,21,1/6/21/23,3,Μπαλαρίνες',
            '24,21,1/6/21/24,3,Πέδιλα',
            '25,21,1/6/21/25,3,Αθλητικά - Sneakers',
//            '27,6,1/6/27,2,Αξεσουάρ',
            '30,3,1/6/3/30,3,Νυφικά',
            '32,3,1/6/3/32,3,Μοκασίνια',
            '33,3,1/6/3/33,3,Παντόφλες',
//            '34,1,1/34,1,ΔΗΜΟΦΙΛΕΣΤΕΡΑ (ΑΡΧΙΚΗ)',
            '39,6,1/6/39,2,Νέες Παραλαβές',
            '40,14,1/6/14/40,3,Σαγιονάρες - Παντόφλες',
            '41,21,1/6/21/41,3,Σαγιονάρες - Παντόφλες',
            '42,21,1/6/21/42,3,Casual',
//            '43,1,1/43,1,Προώθηση (Προϊόν)',
//            '44,1,1/44,1,Προώθηση (Καλάθι)',
//            '45,1,1/45,1,ΝΕΑ ΠΡΟΪΟΝΤΑ (ΑΡΧΙΚΗ)',
            '46,6,1/6/46,2,BAZAAR',
            '47,6,1/6/47,2,Τσάντες-Αξεσουάρ',
            '48,39,1/6/39/48,3,Γυναικεία',
            '49,48,1/6/39/48/49,4,Γόβες',
            '51,48,1/6/39/48/51,4,Πέδιλα',
            '52,48,1/6/39/48/52,4,Μπαλαρίνες',
            '53,48,1/6/39/48/53,4,Casual',
            '55,48,1/6/39/48/55,4,Σαγιονάρες',
            '56,48,1/6/39/48/56,4,Πλατφόρμες',
            '57,48,1/6/39/48/57,4,Μπότες',
            '58,48,1/6/39/48/58,4,Μποτάκια',
            '59,48,1/6/39/48/59,4,Νυφικά',
            '60,48,1/6/39/48/60,4,Μοκασίνια',
            '61,48,1/6/39/48/61,4,Παντόφλες',
            '62,39,1/6/39/62,3,Ανδρικά',
            '63,62,1/6/39/62/63,4,Μποτάκια',
            '64,62,1/6/39/62/64,4,Μπότες',
            '65,62,1/6/39/62/65,4,Μοκασίνια - Loafers',
            '66,62,1/6/39/62/66,4,Σανδάλια',
            '67,62,1/6/39/62/67,4,Casual',
            '68,62,1/6/39/62/68,4,Αθλητικά',
            '69,62,1/6/39/62/69,4,Σαγιονάρες - Παντόφλες',
            '70,39,1/6/39/70,3,Παιδικά',
            '71,70,1/6/39/70/71,4,Μπότες - Μποτάκια',
            '72,70,1/6/39/70/72,4,Μπαλαρίνες',
            '73,70,1/6/39/70/73,4,Πέδιλα',
            '74,70,1/6/39/70/74,4,Αθλητικά - Sneakers',
            '75,70,1/6/39/70/75,4,Σαγιονάρες - Παντόφλες',
            '76,70,1/6/39/70/76,4,Casual',
            '77,62,1/6/39/62/77,4,Πέδιλα- Σανδάλια',
            '78,14,1/6/14/78,3,Πέδιλα- Σανδάλια',
            '79,3,1/6/3/79,3,Σανδάλια',
            '80,48,1/6/39/48/80,4,Σανδάλια',
            '83,21,1/6/21/83,3,Πλατφόρμες',
            '84,70,1/6/39/70/84,4,Πλατφόρμες',
            '85,48,1/6/39/48/85,4,Αθλητικά',
            '87,3,1/6/3/87,3,Αθλητικά',
            '88,14,1/6/14/88,3,Βραδινά',
            '89,62,1/6/39/62/89,4,Βραδινά',
            '90,3,1/6/3/90,3,Γαλότσες',
            '92,57,1/6/39/48/57/92,5,Γαλότσες',
            '96,21,1/6/21/96,3,Μοκασίνια',
            '97,70,1/6/39/70/97,4,Μοκασίνια',
            '110,47,1/6/47/110,3,Κάλτσες',
            '122,46,1/6/46/122,3,Γυναικεία',
            '124,122,1/6/46/122/124,4,Γόβες',
            '125,122,1/6/46/122/125,4,Μπότες',
            '126,122,1/6/46/122/126,4,Μποτάκια',
            '127,122,1/6/46/122/127,4,Casual',
            '128,122,1/6/46/122/128,4,Αθλητικά',
            '129,122,1/6/46/122/129,4,Μπαλαρίνες',
            '130,122,1/6/46/122/130,4,Μοκασίνια',
            '131,122,1/6/46/122/131,4,Παντόφλες Σπιτιού',
            '132,122,1/6/46/122/132,4,Πλατφόρμες',
            '133,122,1/6/46/122/133,4,Νυφικά',
            '134,122,1/6/46/122/134,4,Γαλότσες',
            '135,122,1/6/46/122/135,4,Πέδιλα',
            '136,122,1/6/46/122/136,4,Σανδάλια',
            '137,122,1/6/46/122/137,4,Σαγιονάρες',
            '138,122,1/6/46/122/138,4,Παντόφλες',
            '140,46,1/6/46/140,3,Ανδρικά',
            '141,140,1/6/46/140/141,4,Casual',
            '142,140,1/6/46/140/142,4,Αθλητικά',
            '143,140,1/6/46/140/143,4,Μποτάκια',
            '144,140,1/6/46/140/144,4,Μοκασίνια - Loafers',
            '145,140,1/6/46/140/145,4,Σανδάλια',
            '146,140,1/6/46/140/146,4,Σαγιονάρες - Παντόφλες',
            '147,140,1/6/46/140/147,4,Πέδιλα',
            '148,140,1/6/46/140/148,4,Βραδινά',
            '149,140,1/6/46/140/149,4,Μπότες',
            '150,46,1/6/46/150,3,Παιδικά',
            '151,150,1/6/46/150/151,4,Casual',
            '152,150,1/6/46/150/152,4,Αθλητικά',
            '153,150,1/6/46/150/153,4,Μπαλαρίνες',
            '154,150,1/6/46/150/154,4,Μπότες - Μποτάκια',
            '155,150,1/6/46/150/155,4,Μοκασίνια',
            '156,150,1/6/46/150/156,4,Πέδιλα',
            '157,150,1/6/46/150/157,4,Σαγιονάρες - Παντόφλες',
            '158,150,1/6/46/150/158,4,Πλατφόρμες',
//            '159,1,1/159,1,Νυφικα',
//            '160,6,1/6/160,2,Σανδάλια έως και -60%',
            '161,48,1/6/39/48/161,4,Sneakers - Αθλητικά',
            '162,48,1/6/39/48/162,4,Εσπαντρίγιες',
            '163,62,1/6/39/62/163,4,Sneakers - Αθλητικά',
            '164,62,1/6/39/62/164,4,Εσπαντρίγιες',
            '165,3,1/6/3/165,3,Εσπαντρίγιες',
            '166,3,1/6/3/166,3,Sneakers - Αθλητικά',
            '167,14,1/6/14/167,3,Sneakers - Αθλητικά',
            '169,14,1/6/14/169,3,Εσπαντρίγιες',
            '170,122,1/6/46/122/170,4,Sneakers',
            '171,122,1/6/46/122/171,4,Εσπαντρίγιες',
            '172,140,1/6/46/140/172,4,Sneakers - Αθλητικά',
            '173,140,1/6/46/140/173,4,Εσπαντρίγιες',
            '174,47,1/6/47/174,3,Ζώνες',
            '179,47,1/6/47/179,3,Πορτοφόλια',
            '180,47,1/6/47/180,3,Σακίδια Πλάτης - Backpack',
            '181,47,1/6/47/181,3,Φάκελοι - Clutch',
            '182,47,1/6/47/182,3,Τσάντες Ώμου',
            '183,47,1/6/47/183,3,Τσάντες Χειρός',
            '184,6,1/6/184,2,Luxury',
            '185,184,1/6/184/185,3,Γυναικεία Παπούτσια',
            '186,46,1/6/46/186,3,Tελευταία νούμερα',
//            '187,1,1/187,1,Lastsizes',
            '188,48,1/6/39/48/188,4,Oxfords - Slip On',
            '189,3,1/6/3/189,3,Oxfords - Slip On',
            '190,122,1/6/46/122/190,4,Oxfords - Slip On',
            '193,184,1/6/184/193,3,Ανδρικά Παπούτσια',
            '194,184,1/6/184/194,3,Τσάντες - Πορτοφόλια',
            '195,70,1/6/39/70/195,4,Γαλότσες',
            '196,21,1/6/21/196,3,Γαλότσες',
//            '197,6,1/6/197,2,BLACK FRIDAY',
//            '198,1,1/198,1,excludeFromSkroutz',
            '199,185,1/6/184/185/199,4,Νυφικά Παπούτσια',
            '200,185,1/6/184/185/200,4,Πέδιλα',
            '201,185,1/6/184/185/201,4,Σανδάλια',
            '202,185,1/6/184/185/202,4,Σαγιονάρες',
            '203,185,1/6/184/185/203,4,Εσπαντρίγιες',
            '204,185,1/6/184/185/204,4,Sneakers',
            '205,185,1/6/184/185/205,4,Γόβες',
            '206,185,1/6/184/185/206,4,Oxford - Slip On',
            '207,185,1/6/184/185/207,4,Μπότες - Μποτάκια',
            '208,193,1/6/184/193/208,4,Sneakers',
            '209,193,1/6/184/193/209,4,Μοκασίνια - Loafers',
            '210,193,1/6/184/193/210,4,Σαγιονάρες - Παντόφλες',
            '211,193,1/6/184/193/211,4,Μποτάκια',
            '212,194,1/6/184/194/212,4,Σακίδια Πλάτης - BackPack',
            '213,194,1/6/184/194/213,4,Τσάντες ώμου',
            '214,194,1/6/184/194/214,4,Τσάντες Χειρός',
            '215,194,1/6/184/194/215,4,Φάκελοι - Clutch',
            '216,194,1/6/184/194/216,4,Πορτοφόλια',
            '217,185,1/6/184/185/217,4,Πλατφόρμες',
            '218,185,1/6/184/185/218,4,Μπαλαρίνες',
            '219,47,1/6/47/219,3,Ομπρέλες',
            '220,194,1/6/184/194/220,4,Μπρελόκ',
            '221,3,1/6/3/221,3,Mules',
            '222,48,1/6/39/48/222,4,Mules',
            '223,122,1/6/46/122/223,4,Mules',
            '224,194,1/6/184/194/224,4,Ζώνες',
            '233,39,1/6/39/233,3,Τσάντες - Αξεσουάρ',
            '234,233,1/6/39/233/234,4,Κάλτσες',
            '235,233,1/6/39/233/235,4,Ζώνες',
            '236,233,1/6/39/233/236,4,Τσάντες Χειρός',
            '237,233,1/6/39/233/237,4,Τσάντες Ώμου',
            '238,233,1/6/39/233/238,4,Φάκελοι - Clutch',
            '239,233,1/6/39/233/239,4,Σακίδια Πλάτης - Backpack',
            '240,233,1/6/39/233/240,4,Πορτοφόλια',
            '241,233,1/6/39/233/241,4,Ομπρέλες',
            '242,47,1/6/47/242,3,Προϊόντα περιποίησης υποδημάτων',
            '243,233,1/6/39/233/243,4,Μπρελόκ',
            '244,47,1/6/47/244,3,Μπρελόκ',
            '245,194,1/6/184/194/245,4,Μπρελόκ'
        ];

        $cateogries = [
            ["3","6","1/6/3","2","Γυναικεία","gynaikeia-papoutsia.html"],
["4","3","1/6/3/4","3","Γόβες","gynaikeia-papoutsia/gobes.html"],
["5","3","1/6/3/5","3","Πέδιλα","gynaikeia-papoutsia/pedila.html"],
["7","3","1/6/3/7","3","Μπαλαρίνες","gynaikeia-papoutsia/mpalarines.html"],
["8","3","1/6/3/8","3","Casual","gynaikeia-papoutsia/casual.html"],
["9","3","1/6/3/9","3","Σαγιονάρες","gynaikeia-papoutsia/sagionares.html"],
["10","3","1/6/3/10","3","Πλατφόρμες","gynaikeia-papoutsia/platformes.html"],
["12","3","1/6/3/12","3","Μπότες","gynaikeia-papoutsia/mpotes.html"],
["13","3","1/6/3/13","3","Μποτάκια","gynaikeia-papoutsia/mpotakia.html"],
["14","6","1/6/14","2","Ανδρικά","andrika-papoutsia.html"],
["15","14","1/6/14/15","3","Μποτάκια","andrika-papoutsia/mpotakia.html"],
["16","14","1/6/14/16","3","Μπότες","andrika-papoutsia/mpotes.html"],
["17","14","1/6/14/17","3","Μοκασίνια - Loafers","andrika-papoutsia/mokasinia.html"],
["18","14","1/6/14/18","3","Σανδάλια","andrika-papoutsia/sandalia.html"],
["19","14","1/6/14/19","3","Casual","andrika-papoutsia/casual.html"],
["20","14","1/6/14/20","3","Αθλητικά","andrika-papoutsia/athlitika.html"],
["21","6","1/6/21","2","Παιδικά","paidika-papoutsia.html"],
["22","21","1/6/21/22","3","Μπότες - Μποτάκια","paidika-papoutsia/mpotes-mpotakia.html"],
["23","21","1/6/21/23","3","Μπαλαρίνες ","paidika-papoutsia/mpalarines.html"],
["24","21","1/6/21/24","3","Πέδιλα","paidika-papoutsia/pedila.html"],
["25","21","1/6/21/25","3","Αθλητικά - Sneakers","paidika-papoutsia/athlitika-sneakers.html"],
["27","6","1/6/27","2","Αξεσουάρ","accessories.html"],
["30","3","1/6/3/30","3","Νυφικά","gynaikeia-papoutsia/nyfika.html"],
["32","3","1/6/3/32","3","Μοκασίνια","gynaikeia-papoutsia/gynaikeia-mokasinia.html"],
["33","3","1/6/3/33","3","Παντόφλες","gynaikeia-papoutsia/gynaikeies-pantofles.html"],
["39","6","1/6/39","2","Νέες Παραλαβές","new-products.html"],
["40","14","1/6/14/40","3","Σαγιονάρες - Παντόφλες","andrika-papoutsia/sagionares-pantofles.html"],
["41","21","1/6/21/41","3","Σαγιονάρες - Παντόφλες","paidika-papoutsia/sagionares-pantofles.html"],
["42","21","1/6/21/42","3","Casual","paidika-papoutsia/casual.html"],
["46","6","1/6/46","2","BAZAAR","bazaar.html"],
["47","6","1/6/47","2","Τσάντες-Αξεσουάρ","gynaikeies-tsantes-accesories.html"],
["48","39","1/6/39/48","3","Γυναικεία","new-products/nea-gynaikeia.html"],
["49","48","1/6/39/48/49","4","Γόβες","new-products/nea-gynaikeia/nees-goves.html"],
["51","48","1/6/39/48/51","4","Πέδιλα","new-products/nea-gynaikeia/nea-pedila.html"],
["52","48","1/6/39/48/52","4","Μπαλαρίνες","new-products/nea-gynaikeia/nees-mpalarines.html"],
["53","48","1/6/39/48/53","4","Casual","new-products/nea-gynaikeia/nea-casual.html"],
["55","48","1/6/39/48/55","4","Σαγιονάρες","new-products/nea-gynaikeia/nees-sagionares.html"],
["56","48","1/6/39/48/56","4","Πλατφόρμες","new-products/nea-gynaikeia/nees-platformes.html"],
["57","48","1/6/39/48/57","4","Μπότες","new-products/nea-gynaikeia/nees-mpotes.html"],
["58","48","1/6/39/48/58","4","Μποτάκια","new-products/nea-gynaikeia/nea-mpotakia.html"],
["59","48","1/6/39/48/59","4","Νυφικά","new-products/nea-gynaikeia/nea-nyfika.html"],
["60","48","1/6/39/48/60","4","Μοκασίνια","new-products/nea-gynaikeia/nea-mokasinia.html"],
["61","48","1/6/39/48/61","4","Παντόφλες","new-products/nea-gynaikeia/nees-pantofles.html"],
["62","39","1/6/39/62","3","Ανδρικά","new-products/nea-andrika.html"],
["63","62","1/6/39/62/63","4","Μποτάκια","new-products/nea-andrika/nea-mpotakia.html"],
["64","62","1/6/39/62/64","4","Μπότες","new-products/nea-andrika/nees-mpotes.html"],
["65","62","1/6/39/62/65","4","Μοκασίνια - Loafers","new-products/nea-andrika/nea-mokasinia.html"],
["66","62","1/6/39/62/66","4","Σανδάλια","new-products/nea-andrika/nea-sandalia.html"],
["67","62","1/6/39/62/67","4","Casual","new-products/nea-andrika/nea-casual.html"],
["68","62","1/6/39/62/68","4","Αθλητικά","new-products/nea-andrika/nea-athlitika.html"],
["69","62","1/6/39/62/69","4","Σαγιονάρες - Παντόφλες","new-products/nea-andrika/nees-sagionares-pantofles.html"],
["70","39","1/6/39/70","3","Παιδικά","new-products/nea-paidika.html"],
["71","70","1/6/39/70/71","4","Μπότες - Μποτάκια","new-products/nea-paidika/nea-mpotes-mpotakia.html"],
["72","70","1/6/39/70/72","4","Μπαλαρίνες","new-products/nea-paidika/nees-mpalarines.html"],
["73","70","1/6/39/70/73","4","Πέδιλα","new-products/nea-paidika/nea-pedila.html"],
["74","70","1/6/39/70/74","4","Αθλητικά - Sneakers","new-products/nea-paidika/nea-athlitika-sneakers.html"],
["75","70","1/6/39/70/75","4","Σαγιονάρες - Παντόφλες","new-products/nea-paidika/nees-sagionares-pantofles.html"],
["76","70","1/6/39/70/76","4","Casual","new-products/nea-paidika/nea-casual.html"],
["77","62","1/6/39/62/77","4","Πέδιλα- Σανδάλια","new-products/nea-andrika/nea-pedila-sandalia.html"],
["78","14","1/6/14/78","3","Πέδιλα- Σανδάλια","andrika-papoutsia/pedila-sandalia.html"],
["79","3","1/6/3/79","3","Σανδάλια","gynaikeia-papoutsia/sandalia.html"],
["80","48","1/6/39/48/80","4","Σανδάλια","new-products/nea-gynaikeia/nea-sandalia.html"],
["83","21","1/6/21/83","3","Πλατφόρμες","paidika-papoutsia/platformes.html"],
["84","70","1/6/39/70/84","4","Πλατφόρμες","new-products/nea-paidika/nees-platformes.html"],
["85","48","1/6/39/48/85","4","Αθλητικά","new-products/nea-gynaikeia/nea-athlitika.html"],
["87","3","1/6/3/87","3","Αθλητικά","gynaikeia-papoutsia/athlitika.html"],
["88","14","1/6/14/88","3","Βραδινά","andrika-papoutsia/vradina.html"],
["89","62","1/6/39/62/89","4","Βραδινά","new-products/nea-andrika/nea-vradina.html"],
["90","3","1/6/3/90","3","Γαλότσες","gynaikeia-papoutsia/galotses.html"],
["92","57","1/6/39/48/57/92","5","Γαλότσες","new-products/nea-gynaikeia/nees-mpotes/nees-galotses.html"],
["96","21","1/6/21/96","3","Μοκασίνια","paidika-papoutsia/mokasinia.html"],
["97","70","1/6/39/70/97","4","Μοκασίνια","new-products/nea-paidika/nea-mokasinia.html"],
["110","47","1/6/47/110","3","Κάλτσες","gynaikeies-tsantes-accesories/socks.html"],
["122","46","1/6/46/122","3","Γυναικεία","bazaar/prosfores-gynaikeia.html"],
["124","122","1/6/46/122/124","4","Γόβες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-goves.html"],
["125","122","1/6/46/122/125","4","Μπότες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-mpotes.html"],
["126","122","1/6/46/122/126","4","Μποτάκια","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-mpotakia.html"],
["127","122","1/6/46/122/127","4","Casual","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-casual-papoutsia.html"],
["128","122","1/6/46/122/128","4","Αθλητικά","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-athlitika-papoutsia.html"],
["129","122","1/6/46/122/129","4","Μπαλαρίνες","bazaar/prosfores-gynaikeia/prosfores-gyneikeies-mpalarines.html"],
["130","122","1/6/46/122/130","4","Μοκασίνια","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-mokasinia.html"],
["131","122","1/6/46/122/131","4","Παντόφλες Σπιτιού","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-pantofles-spitiou.html"],
["132","122","1/6/46/122/132","4","Πλατφόρμες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-platformes.html"],
["133","122","1/6/46/122/133","4","Νυφικά","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-nyfika-papoutsia.html"],
["134","122","1/6/46/122/134","4","Γαλότσες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-galotses.html"],
["135","122","1/6/46/122/135","4","Πέδιλα","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-pedila.html"],
["136","122","1/6/46/122/136","4","Σανδάλια","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-sandalia.html"],
["137","122","1/6/46/122/137","4","Σαγιονάρες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-sagionares.html"],
["138","122","1/6/46/122/138","4","Παντόφλες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-pantofles.html"],
["140","46","1/6/46/140","3","Ανδρικά","bazaar/prosfores-andrika.html"],
["141","140","1/6/46/140/141","4","Casual","bazaar/prosfores-andrika/prosfores-andrika-casual.html"],
["142","140","1/6/46/140/142","4","Αθλητικά","bazaar/prosfores-andrika/prosfores-adrika-athlitika.html"],
["143","140","1/6/46/140/143","4","Μποτάκια","bazaar/prosfores-andrika/prosfores-andrika-mpotakia.html"],
["144","140","1/6/46/140/144","4","Μοκασίνια - Loafers","bazaar/prosfores-andrika/prosfores-andrika-mokasinia-loafers.html"],
["145","140","1/6/46/140/145","4","Σανδάλια","bazaar/prosfores-andrika/prosfores-andrika-sandalia.html"],
["146","140","1/6/46/140/146","4","Σαγιονάρες - Παντόφλες","bazaar/prosfores-andrika/prosfores-andrikes-sagionares-pantofles.html"],
["147","140","1/6/46/140/147","4","Πέδιλα","bazaar/prosfores-andrika/prosfores-andrika-pedila.html"],
["148","140","1/6/46/140/148","4","Βραδινά","bazaar/prosfores-andrika/prosfores-andrika-vradina-papoutsia.html"],
["149","140","1/6/46/140/149","4","Μπότες","bazaar/prosfores-andrika/prosfores-andrikes-mpotes.html"],
["150","46","1/6/46/150","3","Παιδικά","bazaar/prosfores-paidika.html"],
["151","150","1/6/46/150/151","4","Casual","bazaar/prosfores-paidika/prosfores-paidika-casual-papoutsia.html"],
["152","150","1/6/46/150/152","4","Αθλητικά","bazaar/prosfores-paidika/prosfores-paidika-athlitika-papoutsia.html"],
["153","150","1/6/46/150/153","4","Μπαλαρίνες","bazaar/prosfores-paidika/prosfores-paidikes-mpalarines.html"],
["154","150","1/6/46/150/154","4","Μπότες - Μποτάκια","bazaar/prosfores-paidika/prosfores-paidika-mpotes-mpotakia.html"],
["155","150","1/6/46/150/155","4","Μοκασίνια","bazaar/prosfores-paidika/prosfores-paidika-mokasinia.html"],
["156","150","1/6/46/150/156","4","Πέδιλα","bazaar/prosfores-paidika/prosfores-paidika-pedila.html"],
["157","150","1/6/46/150/157","4","Σαγιονάρες - Παντόφλες","bazaar/prosfores-paidika/prosfores-paidikes-sagionares-pantofles.html"],
["158","150","1/6/46/150/158","4","Πλατφόρμες","bazaar/prosfores-paidika/prosfores-paidikes-platformes.html"],
["160","6","1/6/160","2","Σανδάλια έως και -60%","sandalia-up-to-60.html"],
["161","48","1/6/39/48/161","4","Sneakers - Αθλητικά","new-products/nea-gynaikeia/nea-sneakers.html"],
["162","48","1/6/39/48/162","4","Εσπαντρίγιες","new-products/nea-gynaikeia/nees-espantrigies.html"],
["163","62","1/6/39/62/163","4","Sneakers - Αθλητικά","new-products/nea-andrika/nea-sneakers.html"],
["164","62","1/6/39/62/164","4","Εσπαντρίγιες","new-products/nea-andrika/nees-espantrigies.html"],
["165","3","1/6/3/165","3","Εσπαντρίγιες","gynaikeia-papoutsia/espantrigies.html"],
["166","3","1/6/3/166","3","Sneakers - Αθλητικά","gynaikeia-papoutsia/sneakers.html"],
["167","14","1/6/14/167","3","Sneakers - Αθλητικά","andrika-papoutsia/sneakers.html"],
["169","14","1/6/14/169","3","Εσπαντρίγιες","andrika-papoutsia/espantrigies.html"],
["170","122","1/6/46/122/170","4","Sneakers","bazaar/prosfores-gynaikeia/prosfores-gynaikeia-sneakers.html"],
["171","122","1/6/46/122/171","4","Εσπαντρίγιες","bazaar/prosfores-gynaikeia/prosfores-gynaikeies-espantrigies.html"],
["172","140","1/6/46/140/172","4","Sneakers - Αθλητικά","bazaar/prosfores-andrika/prosfores-andrika-sneakers.html"],
["173","140","1/6/46/140/173","4","Εσπαντρίγιες","bazaar/prosfores-andrika/prosfores-andrika-espantrikes.html"],
["174","47","1/6/47/174","3","Ζώνες","gynaikeies-tsantes-accesories/zwnes.html"],
["179","47","1/6/47/179","3","Πορτοφόλια","gynaikeies-tsantes-accesories/portofolia.html"],
["180","47","1/6/47/180","3","Σακίδια Πλάτης - Backpack","gynaikeies-tsantes-accesories/sakidia-platis-packpack.html"],
["181","47","1/6/47/181","3","Φάκελοι - Clutch","gynaikeies-tsantes-accesories/fakeloi-clutch.html"],
["182","47","1/6/47/182","3","Τσάντες Ώμου","gynaikeies-tsantes-accesories/tsantes-omou.html"],
["183","47","1/6/47/183","3","Τσάντες Χειρός","gynaikeies-tsantes-accesories/tsantes-xeiros.html"],
["184","6","1/6/184","2","Luxury","luxury.html"],
["185","184","1/6/184/185","3","Γυναικεία Παπούτσια","luxury/luxury-gynaikeia-papoutsia.html"],
["186","46","1/6/46/186","3","Tελευταία νούμερα ","bazaar/last-sizes.html"],
["188","48","1/6/39/48/188","4","Oxfords - Slip On","new-products/nea-gynaikeia/oxfords-slip-on.html"],
["189","3","1/6/3/189","3","Oxfords - Slip On","gynaikeia-papoutsia/oxfords-slip-on.html"],
["190","122","1/6/46/122/190","4","Oxfords - Slip On","bazaar/prosfores-gynaikeia/bazaar-oxfords-slip-on.html"],
["193","184","1/6/184/193","3","Ανδρικά Παπούτσια","luxury/luxury-andrika-papoutsia.html"],
["194","184","1/6/184/194","3","Τσάντες - Πορτοφόλια","luxury/luxury-tsantes-portofolia.html"],
["195","70","1/6/39/70/195","4","Γαλότσες","new-products/nea-paidika/new-products-paidika-galotses.html"],
["196","21","1/6/21/196","3","Γαλότσες","paidika-papoutsia/paidika-papoutsia-galotses.html"],
["197","6","1/6/197","2","BLACK FRIDAY","black-friday.html"],
["199","185","1/6/184/185/199","4","Νυφικά Παπούτσια","luxury/luxury-gynaikeia-papoutsia/nifika-papoutsia.html"],
["200","185","1/6/184/185/200","4","Πέδιλα","luxury/luxury-gynaikeia-papoutsia/pedila-216.html"],
["201","185","1/6/184/185/201","4","Σανδάλια","luxury/luxury-gynaikeia-papoutsia/sandalia-217.html"],
["202","185","1/6/184/185/202","4","Σαγιονάρες","luxury/luxury-gynaikeia-papoutsia/sagionarew-218.html"],
["203","185","1/6/184/185/203","4","Εσπαντρίγιες","luxury/luxury-gynaikeia-papoutsia/espantrigiew-219.html"],
["204","185","1/6/184/185/204","4","Sneakers","luxury/luxury-gynaikeia-papoutsia/sneakers-220.html"],
["205","185","1/6/184/185/205","4","Γόβες","luxury/luxury-gynaikeia-papoutsia/gobew-221.html"],
["206","185","1/6/184/185/206","4","Oxford - Slip On","luxury/luxury-gynaikeia-papoutsia/oxford-slip-on-222.html"],
["207","185","1/6/184/185/207","4","Μπότες - Μποτάκια","luxury/luxury-gynaikeia-papoutsia/mpotew-mpotakia-223.html"],
["208","193","1/6/184/193/208","4","Sneakers","luxury/luxury-andrika-papoutsia/sneakers-225.html"],
["209","193","1/6/184/193/209","4","Μοκασίνια - Loafers","luxury/luxury-andrika-papoutsia/mokasinia-loafers-226.html"],
["210","193","1/6/184/193/210","4","Σαγιονάρες - Παντόφλες","luxury/luxury-andrika-papoutsia/sagionarew-227.html"],
["211","193","1/6/184/193/211","4","Μποτάκια","luxury/luxury-andrika-papoutsia/mpotakia-228.html"],
["212","194","1/6/184/194/212","4","Σακίδια Πλάτης - BackPack","luxury/luxury-tsantes-portofolia/sakidia-plathw-backpack-229.html"],
["213","194","1/6/184/194/213","4","Τσάντες ώμου","luxury/luxury-tsantes-portofolia/tsantew-vmoy-230.html"],
["214","194","1/6/184/194/214","4","Τσάντες Χειρός","luxury/luxury-tsantes-portofolia/tsantew-xeirow-231.html"],
["215","194","1/6/184/194/215","4","Φάκελοι - Clutch","luxury/luxury-tsantes-portofolia/fakeloi-clutch-232.html"],
["216","194","1/6/184/194/216","4","Πορτοφόλια","luxury/luxury-tsantes-portofolia/portofolia-233.html"],
["217","185","1/6/184/185/217","4","Πλατφόρμες","luxury/luxury-gynaikeia-papoutsia/platformew-235.html"],
["218","185","1/6/184/185/218","4","Μπαλαρίνες","luxury/luxury-gynaikeia-papoutsia/mpalarinew-236.html"],
["219","47","1/6/47/219","3","Ομπρέλες","gynaikeies-tsantes-accesories/umbrellas-237.html"],
["220","194","1/6/184/194/220","4","Μπρελόκ","luxury/luxury-tsantes-portofolia/mprelok-238.html"],
["221","3","1/6/3/221","3","Mules","gynaikeia-papoutsia/mules-241.html"],
["222","48","1/6/39/48/222","4","Mules","new-products/nea-gynaikeia/mules-240.html"],
["223","122","1/6/46/122/223","4","Mules","bazaar/prosfores-gynaikeia/mules-242.html"],
["224","194","1/6/184/194/224","4","Ζώνες","luxury/luxury-tsantes-portofolia/zvnew-243.html"],
["233","39","1/6/39/233","3","Τσάντες - Αξεσουάρ","new-products/tsantew-ajesoyar-244.html"],
["234","233","1/6/39/233/234","4","Κάλτσες","new-products/tsantew-ajesoyar-244/kaltsew-245.html"],
["235","233","1/6/39/233/235","4","Ζώνες","new-products/tsantew-ajesoyar-244/zvnew-246.html"],
["236","233","1/6/39/233/236","4","Τσάντες Χειρός","new-products/tsantew-ajesoyar-244/tsantew-xeirow-247.html"],
["237","233","1/6/39/233/237","4","Τσάντες Ώμου","new-products/tsantew-ajesoyar-244/tsantew-vmoy-248.html"],
["238","233","1/6/39/233/238","4","Φάκελοι - Clutch","new-products/tsantew-ajesoyar-244/fakeloi-clutch-249.html"],
["239","233","1/6/39/233/239","4","Σακίδια Πλάτης - Backpack","new-products/tsantew-ajesoyar-244/sakidia-plathw-backpack-250.html"],
["240","233","1/6/39/233/240","4","Πορτοφόλια","new-products/tsantew-ajesoyar-244/portofolia-251.html"],
["241","233","1/6/39/233/241","4","Ομπρέλες","new-products/tsantew-ajesoyar-244/umbrellas-252.html"],
["242","47","1/6/47/242","3","Προϊόντα περιποίησης υποδημάτων","gynaikeies-tsantes-accesories/proionta-peripoihshw-ypodhmatvn-253.html"],
["243","233","1/6/39/233/243","4","Μπρελόκ","new-products/tsantew-ajesoyar-244/mprelok-255.html"],
["244","47","1/6/47/244","3","Μπρελόκ","gynaikeies-tsantes-accesories/mprelok-256.html"],
["245","194","1/6/184/194/245","4","Μπρελόκ","luxury/luxury-tsantes-portofolia/mprelok-257.html"],

        ];
        $erpCategories = ['31,	1,	Root Catalog',
            '35,	6,	e-shoes.gr',
            '32,	3,	Γυναικεία',
            '33,	4,	Γόβες ',
            '34,	5,	Πέδιλα',
            '36,	7,	Μπαλαρίνες',
            '37,	8,	Casual',
            '38,	9,	Σαγιονάρες',
            '40,	12,	Μπότες',
            '41,	13,	Μποτάκια',
            '55,	30,	Νυφικά',
            '56,	32,	Μοκασίνια',
            '57,	33,	Παντόφλες',
            '93,	79,	Σανδάλια',
            '95,	81,	Jellies',
            '100,	87,	Αθλητικά',
            '103,	90,	Γαλότσες',
            '110,	109,	Παντόφλες Σπιτιού',
            '161,	166,	Sneakers',
            '181,	10,	Πλατφόρμες',
            '182,	165,	Εσπαντρίγιες',
            '198,	189,	Oxfords - Slip On',
            '241,	221,	Mules',
            '42,	14,	Ανδρικά',
            '43,	15,	Μποτάκια',
            '44,	16,	Μπότες',
            '45,	17,	Μοκασίνια - Loafers',
            '46,	18,	Σανδάλια',
            '47,	19,	Casual',
            '48,	20,	Αθλητικά',
            '59,	40,	Σαγιονάρες - Παντόφλες',
            '92,	78,	Πέδιλα',
            '101,	88,	Βραδινά',
            '162,	167,	Sneakers',
            '163,	169,	Εσπαντρίγιες',
            '49,	21,	Παιδικά',
            '50,	22,	Μπότες - Μποτάκια',
            '51,	23,	Μπαλαρίνες ',
            '52,	24,	Πέδιλα',
            '53,	25,	Αθλητικά',
            '60,	41,	Σαγιονάρες - Παντόφλες',
            '61,	42,	Casual',
            '97,	83,	Πλατφόρμες',
            '107,	96,	Μοκασίνια',
            '215,	196,	Γαλότσες',
            '54,	27,	Αξεσουάρ',
            '105,	94,	Γυναικεία Ρολόγια',
            '106,	95,	Παιδικά Καπέλα',
            '58,	39,	Νέες Παραλαβές',
            '64,	48,	Γυναικεία',
            '65,	49,	Γόβες',
            '66,	51,	Πέδιλα',
            '67,	52,	Μπαλαρίνες',
            '68,	53,	Casual',
            '69,	55,	Σαγιονάρες',
            '70,	56,	Πλατφόρμες',
            '71,	57,	Μπότες',
            '72,	58,	Μποτάκια',
            '73,	59,	Νυφικά',
            '74,	60,	Μοκασίνια',
            '75,	61,	Παντόφλες',
            '94,	80,	Σανδάλια',
            '96,	82,	Jellies',
            '99,	85,	Αθλητικά',
            '104,	92,	Γαλότσες',
            '109,	108,	Παντόφλες Σπιτιού',
            '156,	161,	Sneakers',
            '157,	162,	Εσπαντρίγιες',
            '199,	188,	Oxfords - Slip On',
            '240,	222,	Mules',
            '76,	62,	Ανδρικά',
            '77,	63,	Μποτάκια',
            '78,	64,	Μπότες',
            '79,	65,	Μοκασίνια - Loafers',
            '80,	66,	Σανδάλια',
            '81,	67,	Casual',
            '82,	68,	Αθλητικά',
            '83,	69,	Σαγιονάρες - Παντόφλες',
            '91,	77,	Πέδιλα',
            '102,	89,	Βραδινά',
            '158,	163,	Sneakers',
            '159,	164,	Εσπαντρίγιες',
            '84,	70,	Παιδικά',
            '85,	71,	Μπότες - Μποτάκια',
            '86,	72,	Μπαλαρίνες',
            '87,	73,	Πέδιλα',
            '88,	74,	Αθλητικά',
            '89,	75,	Σαγιονάρες - Παντόφλες',
            '90,	76,	Casual',
            '98,	84,	Πλατφόρμες',
            '108,	97,	Μοκασίνια',
            '244,	233,	Τσάντες - Αξεσουάρ',
            '245,	234,	Κάλτσες',
            '246,	235,	Ζώνες',
            '247,	236,	Τσάντες Χειρός',
            '248,	237,	Τσάντες Ώμου',
            '249,	238,	Φάκελοι - Clutch',
            '250,	239,	Σακίδια Πλάτης - Backpack',
            '251,	240,	Πορτοφόλια',
            '252,	241,	Ομπρέλες',
            '255,	243,	Μπρελόκ',
            '62,	46,	Προσφορές',
            '119,	122,	Γυναικεία',
            '120,	124,	Γόβες',
            '121,	125,	Μπότες',
            '122,	126,	Μποτάκια',
            '123,	127,	Casual',
            '124,	128,	Αθλητικά',
            '125,	129,	Μπαλαρίνες',
            '126,	130,	Μοκασίνια',
            '127,	131,	Παντόφλες Σπιτιού',
            '128,	132,	Πλατφόρμες',
            '129,	133,	Νυφικά',
            '130,	134,	Γαλότσες',
            '131,	135,	Πέδιλα',
            '132,	136,	Σανδάλια',
            '133,	137,	Σαγιονάρες',
            '134,	138,	Παντόφλες',
            '135,	139,	Jellies',
            '164,	170,	Sneakers',
            '165,	171,	Εσπαντρίγιες',
            '197,	190,	Oxfords - Slip On',
            '242,	223,	Mules',
            '136,	140,	Ανδρικά',
            '137,	141,	Casual',
            '138,	142,	Αθλητικά',
            '139,	143,	Μποτάκια',
            '140,	144,	Μοκασίνια',
            '141,	145,	Σανδάλια',
            '142,	146,	Σαγιονάρες - Παντόφλες',
            '143,	147,	Πέδιλα',
            '144,	148,	Βραδινά',
            '145,	149,	Μπότες',
            '166,	172,	Sneakers',
            '167,	173,	Εσπαντρίγιες',
            '146,	150,	Παιδικά',
            '147,	151,	Casual',
            '148,	152,	Αθλητικά',
            '149,	153,	Μπαλαρίνες',
            '150,	154,	Μπότες - Μποτάκια',
            '151,	155,	Μοκασίνια',
            '152,	156,	Πέδιλα',
            '153,	157,	Σαγιονάρες - Παντόφλες',
            '154,	158,	Πλατφόρμες',
            '169,	175,	Bazaar',
            '170,	176,	Ανδρικά',
            '171,	177,	Γυναικεία',
            '172,	178,	Παιδικά',
            '201,	186,	Τελευταία Νούμερα',
            '63,	47,	Τσάντες-Αξεσουάρ',
            '111,	110,	Κάλτσες',
            '168,	174,	Ζώνες',
            '204,	183,	Τσάντες Χειρός',
            '205,	182,	Τσάντες Ώμου',
            '206,	181,	Φάκελοι - Clutch',
            '207,	180,	Σακίδια Πλάτης - Backpack',
            '208,	179,	Πορτοφόλια',
            '237,	219,	Ομπρέλες',
            '253,	242,	Προϊόντα περιποίησης υποδημάτων',
            '256,	244,	Μπρελόκ',
            '112,	115,	Super Offer',
            '113,	116,	Group 1',
            '114,	117,	Group 2',
            '115,	118,	Group 3',
            '116,	119,	Group 4',
            '117,	120,	Group 5',
            '118,	121,	Group 6',
            '155,	160,	Σανδάλια έως και -60%',
            '202,	184,	Luxury',
            '203,	185,	Γυναικεία Παπούτσια',
            '216,	200,	Πέδιλα',
            '217,	201,	Σανδάλια',
            '218,	202,	Σαγιονάρες',
            '219,	203,	Εσπαντρίγιες',
            '220,	204,	Sneakers',
            '221,	205,	Γόβες',
            '222,	206,	Oxford - Slip On',
            '223,	207,	Μπότες - Μποτάκια',
            '235,	217,	Πλατφόρμες',
            '236,	218,	Μπαλαρίνες',
            '211,	193,	Ανδρικά Παπούτσια',
            '225,	208,	Sneakers',
            '226,	209,	Μοκασίνια - Loafers',
            '227,	210,	Σαγιονάρες - Παντόφλες',
            '228,	211,	Μποτάκια',
            '212,	194,	Τσάντες-Αξεσουάρ',
            '229,	212,	Σακίδια Πλάτης - BackPack',
            '230,	213,	Τσάντες ώμου',
            '231,	214,	Τσάντες Χειρός',
            '232,	215,	Φάκελοι - Clutch',
            '233,	216,	Πορτοφόλια',
            '238,	220,	Μπρελόκ',
            '243,	224,	Ζώνες',
            '257,	245,	Μπρελόκ',
            '173,	159,	Νυφικα',
            '174,	34,	ΔΗΜΟΦΙΛΕΣΤΕΡΑ (ΑΡΧΙΚΗ)',
            '175,	43,	Προώθηση (Προϊόν)',
            '176,	44,	Προώθηση (Καλάθι)',
            '177,	45,	ΝΕΑ ΠΡΟΪΟΝΤΑ (ΑΡΧΙΚΗ)'];



// normalize erp categories
        $newerpCategories = [];

        $newErpcategories = [];
        foreach ($erpCategories as $cat) {

            $ncat = explode(',', $cat);
            $newErpcategories[] = $ncat;
        }


        $buildErpCat = [];
        foreach ($newErpcategories as $cat) {
            $buildErpCat[(integer)$cat[1]] = (integer)$cat[0];
        }


        // normalize simple categories
//        $categories = [];
//        foreach ($cateogries as $cat) {
//
////            $ncat = explode(',', $cat);
//            $categories[] = $ncat;
//        }

        $categories = $cateogries;

        $buildCat = [];
        foreach ($categories as $cat) {

            if(isset($buildErpCat[$cat[0]])){
                $dcat = array_push($cat, $buildErpCat[$cat[0]]);
                $buildCat[$cat[2]] = $cat;
            }


        }



         $categoryTree = $this->explodeTree($buildCat, '/', true);




        $newcategories = [];
        foreach ($categoryTree as $key => $subcategory) {

            if (is_array($subcategory['6'])) {

                foreach ($subcategory['6'] as $skey => $sc) {
                    $erp = $sc['erp'];
                    $name = $sc['name'];
                    $url = $sc['url'];
                    if($name == 'e-shoes.gr')
                        continue;
                    $createdMMother = $ncategory = Category::create(['title' => $name, 'name' => $url, 'active' => 1, 'parent_id' => 0, 'erp_id' => $erp]);
                    $newcategories[$skey] = $createdMMother->id;
                    if(is_array($sc)){
                        foreach ($sc as $key => $mother){
                            if(isset($mother['name']) && $mother['erp']){
                                $justCREATED = Category::create(['title' => (string)$mother['name'], 'name' => (string)$mother['url'], 'active' => 1, 'parent_id' => $createdMMother->id, 'erp_id' => $mother['erp']]);
                                $newcategories[$skey] = $ncategory->id;
                            }

                            if(is_array($mother) && count($mother) > 2){

                                foreach($mother as $children){
                                    if(isset($children['name']) && $children['erp']){
                                        $children  = Category::create(['title' => (string)$children['name'], 'name' => (string)$children['url'], 'active' => 1, 'parent_id' => $justCREATED->id, 'erp_id' => $children['erp']]);
                                    }
                                }
                            }

                        }
                    }else{

                    }
                    if (isset($sc['__base_val'])) {

                    } else {
//
                    }


                }

            }
        }


        return $newcategories;

    }


    function explodeTree($array, $delimiter = '_', $baseval = false)
    {
        if (!is_array($array)) return false;
        $splitRE = '/' . preg_quote($delimiter, '/') . '/';
        $returnArr = array();
        foreach ($array as $key => $val) {
            // Get parent parts and the current leaf
            $parts = preg_split($splitRE, $key, -1, PREG_SPLIT_NO_EMPTY);
            $leafPart = array_pop($parts);

            // Build parent structure
            // Might be slow for really deep and large structures
            $parentArr = &$returnArr;
            foreach ($parts as $part) {
                if (!isset($parentArr[$part])) {
                    $parentArr[$part] = array();
                } elseif (!is_array($parentArr[$part])) {
                    if ($baseval) {
                        $parentArr[$part] = array('__base_val' => $parentArr[$part]);
                    } else {
                        $parentArr[$part] = array();
                    }
                }
                $parentArr = &$parentArr[$part];
            }

            // Add the final part to the structure
            $end = explode('/', $val[5]);
            $url = str_replace('.html', '', end($end));
            if (empty($parentArr[$leafPart])) {

                $parentArr[$leafPart] = ['name'=>$val[4], 'erp'=>$val[6], 'url'=>$url];
            } elseif ($baseval && is_array($parentArr[$leafPart])) {

                $parentArr[$leafPart]['__base_val'] = ['name'=>$val[4], 'erp'=>$val[6], 'url'=>$url];

            }
        }
        return $returnArr;
    }
}
