<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){



        $data_url = '/manage/users_list_json';
        $edit_url = '/manage/users';
        $headers = [
            ['text'=> 'Id', 'value'=> 'id'],
            ['text'=> 'Όνομα', 'value'=> 'name'],
            ['text'=> 'Επίθετο', 'value'=> 'lastname'],
            ['text'=> 'Email', 'value'=> 'email'],
            ['text'=> 'Ρόλος', 'value'=> 'roles[0].name'],
            ['text'=> 'Επιλογές', 'value'=> 'actions']
        ];
        return view('manage.users.index', compact('data_url', 'edit_url', 'headers'));
    }
    public function listJson(Request $request){
        return $products = User::with('roles', 'permissions')->paginate($request->get('paginate'));
    }

    public function show(User $user){
        return view('manage.users.show', compact('user'));
    }
    public function create(){
        $roles = Role::get();
        return view('manage.users.create', compact('roles'));
    }

    public function store(Request $request){


        $user = User::create([
            'name' => $request->user['name'],
            'lastname' => $request->user['lastname'],
            'mobile' => $request->user['mobile'],
            'phone' => "",
            'email' => $request->user['email'],
            "password" => Hash::make($request->user['password']),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);



        $user->assignRole($request->get('roles'));
    }

}
