<?php

namespace App\Http\Controllers;

use App\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function index()
    {

        $edit_url = '/manage/rules';
        $data_url = '/manage/rules_list_json';
        $headers = [
            [
                'text'=> 'Title',
                'align'=> 'start',
                'sortable'=> true,
                'value'=> 'title',
            ],
            ['text'=> 'Επιλογές', 'value'=> 'actions']

        ];

        return view('manage.voucher.index', compact('headers', 'data_url', 'edit_url'));
    }
    public function listJson(Request $request){
        return $products = Voucher::paginate($request->get('paginate'));
    }

    public function create(){
        return view('manage.voucher.create');
    }
}
