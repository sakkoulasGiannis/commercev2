<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Order extends Model
{
    public $fillable = [
        'id',
        'order_num',
        'comment',
        'ip',
        'user_id',
        'order_comment',
        'shipping_id',
        'slug',
        'tracking_number'
    ];

    public function getCreatedAtAttribute($value)
    {
        return date("d f Y H:i", strtotime($value));
    }




    public function products()
    {
        return $this->hasManyThrough(OrderProduct::class, OrderDetail::class, 'order_id', 'order_detail_id', 'id', 'id');
    }

    public function details()
    {
        return $this->hasOne(OrderDetail::class, 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
