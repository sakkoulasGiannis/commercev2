<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Courier;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_date',
        'status',
        'invoice_id',
        'payment_id',
        'product_id',
        'shipping_id',
        'total',
        'sub_total',
        'user_id',
        'billing_name',
        'billing_lastname',
        'billing_country_id',
        'billing_region',
        'billing_city',
        'billing_street',
        'billing_zip',
        'billing_street_number',
        'billing_phone',
        'billing_mobile',
        'shipping_country_id',
        'shipping_region',
        'shipping_city',
        'shipping_street',
        'shipping_zip',
        'shipping_street_number',
        'shipping_phone',
        'shipping_mobile',
        'shipping_name',
        'shipping_lastname',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'id', 'order_id');
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_detail_id', 'id');
    }
}
