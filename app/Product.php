<?php

namespace App;

use App\Field;
use App\Scopes\FilterScope;
use App\Http\Traits\FieldTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\QueueManager;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Laravel\Scout\Searchable;
use Illuminate\Support\Facades\Schema;
use Spatie\Translatable\HasTranslations;

class Product extends Model implements HasMedia
{

    //@todo gdpr
    use HasFactory;
//    use Searchable;
    use InteractsWithMedia;
    use FieldTrait;
    use HasTranslations;

    public $translatable = ['title', 'meta_title'];


    protected $fillable = [
        'title',
        'name',
        'small_description',
        'description',
        'sku',
        'product_type',
        'price',
        'special_price',
        'meta_title',
        'meta_description',
        'rule_id',
        'sort',
        'group',
        'is_featured',
        'is_active',
        'tax_class_id',
        'new_from',
        'new_to'
    ];





    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('product')
            ->width(800)
            ->height(800);

        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(368);
    }



//    public function registerMediaConversions(Media $media = null)
//    {
//        $this->addMediaConversion('thumb')
//            ->width(368)
//            ->height(368)
//            ->performOnCollections('product');
//    }
//
////    public function registerMediaConversions(Media $media = null)
////    {
////        $this->addMediaConversion('thumb')
////            ->fit(Manipulations::FIT_FILL, 368, 368)
////            ->performOnCollections('product');
////    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category')->withTimestamps();
    }

    public function rules()
    {
        return $this->hasOne(Rule::class, 'id', 'rule_id');
    }



    public function stock(){

        return $this->hasMany(Stock::class, 'product_id', 'id')->with('variations');
//        return $this->hasMany(Stock::class)->with('variations');
    }

    public function variants(){
        return $this->hasManyThrough(StockVariation::class, Stock::class);

    }

    public function values(){
        return $this->belongsToMany(FieldValues::class, 'product_values', 'product_id', 'id');
    }

    public function calculator(){
        return $this->hasOne(Calculator::class);
    }


    public function scopeHasStock($query)
    {

        return $query->whereHas('stock', function($query){$query->where('quantity','>' , 0);});

    }


    /**
     * return new price if product has discount
     *
     * @param $value
     * @return mixed
     */
    public function getPriceAttribute($value)
    {
        $this->oldPrice = false;

        if (request()->is('manage/*')) {
            return $value;
        } else {
            if ($this->special_price > 0) {
                $this->originalPrice = $value;
                return $this->special_price;
            } elseif ($this->rules) {
                $discountValue = $this->rules->discount_value;
                $discountType = $this->rules->discount_type;
                $this->oldPrice = true;

                if ($discountType == 2) {
                    $this->discount = 112;
                    $discount = $value - $discountValue;
                    $this->originalPrice = $value;
                    return round($discount);
                }

                if ($discountType == 1) {
                    $finalPrice = $value - ($value * ($discountValue / 100));
                    $this->discount = $this->rules->discount_value;
                    $this->originalPrice = $value;
                    return  round($finalPrice);
                }
            }
            // self::$hasDiscount = 110;
            return $value;
        }
    }


    /*
     * get relative products
     */
    public function scopeGetRelative($query)
    {
        return $query->where('group', $this->group)
            ->where('id', '!=', $this->id)->get();
    }

    public function scopeInStock($query)
    {
        return $query->join('stocks', 'products.id', '=', 'stocks.product_id')
            ->groupBy('products.id')
            ->havingRaw('SUM(stocks.quantity) > 0')
            ->select([
                'products.*',
            ]);
    }

    public function getDiscountAttribute()
    {
        $discount = null;
        if ($this->special_price > 0) {
            $discount = (($this->getOriginal('price') - $this->special_price) / $this->getOriginal('price')) * 100;
        } elseif ($this->rules) {
            $discountValue = $this->rules->discount_value;
            $discountType = $this->rules->discount_type;
            if ($discountType == 1) {
                $discount = $this->rules->discount_value;
            } elseif ($discountType == 2) {
                $discount = (($this->price - $discountValue) / $this->price) * 100;
            }
        }

        return round($discount);
    }

    public function scopeFilter ($query, $field, $name) {
        return $query->whereHas($field, function ($q) use ($name) {
            $q->where('name', $name);
        });
    }


}
