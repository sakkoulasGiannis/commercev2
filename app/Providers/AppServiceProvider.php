<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(Schema::hasTable('categories')){
            View::share('allCategories', Category::where('parent_id', 0)->with('children', 'children.children')->orderBy('order', 'asc')->get());
        }


//        if (\Schema::hasTable('categories')) {
//            View::share('categories', Category::where('parent_id', 0)->orderBy('order', 'asc')->get());
//
//            view()->composer('manage.index', function ($view) {
//                $view->with('products', \App\Product::all());
//            });
//        }
    }
}
