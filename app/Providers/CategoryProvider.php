<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\ServiceProvider;

class CategoryProvider extends ServiceProvider
{

    protected $categories;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->categories = Category::all();
    }

    /**,
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(View $view)
    {
        $view->with('allCategories', $this->Categories);
    }
}
