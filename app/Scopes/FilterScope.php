<?php

namespace App\Scopes;

use App\Field;
use http\Env\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FilterScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        if(!\Request::is('manage/*')
            && !\Request::is('api/*')
            && !\Request::is('calculator_get_price')
            && !\Request::is('add_to_cart_dynamic_product')
            && !\Request::is('add_to_cart')
            && Request()->input()){
//            $fields = Field::pluck('name')->toArray();
//
//            foreach (Request()->input() as $key => $val){
//                if(in_array($key, $fields)){
//                    $builder->whereHas(ucfirst($key), function ( $query) use ($key, $val) {
//                        $query->where('title', $val);
//                    });
//                }
//            }
        }


    }
}
