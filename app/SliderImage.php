<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderImage extends Model
{
    protected $fillable = [ 'url', 'alt', 'href', 'slider_id'];

    public function slider()
    {
        return $this->belongsTo(Slider::class, "id", "slider_id");
    }
}
