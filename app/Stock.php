<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $casts = [
        'quantity'=>'integer',

        'sku'=>'string',
    ];
    protected $fillable = [
        'product_id',
        'sku',
        "ean",
        'barcode',
        'quantity',
        'price',
    ];
    public function product(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function variations(){
        return $this->hasMany(StockVariation::class, 'stock_id', 'id');
    }

    public function fields(){
        return $this->hasManyThrough(Field::class, StockVariation::class);
    }

    public function scopeStockItems($query)
    {
        return $query->each(function($q){
              $q->sku = 'ssdf';
        });
    }
}
