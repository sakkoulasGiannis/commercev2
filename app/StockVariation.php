<?php

namespace App;

use App\Fields\Color;
use Illuminate\Database\Eloquent\Model;

class StockVariation extends Model
{
    protected $fillable = ['stock_id', 'field_id', 'model_id', 'model_type'];

    public function stock(){
        return $this->belongsTo(Stock::class, 'id', 'stock_id');
    }

    public function fields(){
        $this->belongsTo(Field::class, 'id','model_id' );
    }
}
