<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;

trait NationalBankOfGreeceTrait
{
    protected $nbgPassword;
    protected $nbgClient;
    protected $page_set_id;
    protected $backUrl;
    protected $return_url;
    protected $expiry_url;
    protected $error_url;
    protected $nbgUrl;

    public function __construct()
    {
        $this->backUrl = route('checkout.success'); // ********** set to customer page ********
        $this->return_url = route('checkout.success'); // ********** set to customer page ********
        $this->expiry_url = route('checkout.success'); // ********** set to customer page ********
        $this->error_url = route('checkout.error'); // ********** set to customer page ********
 // if demo mode
        if (env('NBG_TEST_MODE')) {
            $this->nbgUrl = 'https://accreditation.datacash.com/Transaction/acq_a';
            $this->nbgPassword = env('NBG_DEMO_PASSWORD');
            $this->nbgClient = env('NBG_DEMO_CLIENT');
        } else {
            $this->nbgUrl = 'https://mars.transaction.datacash.com/Transaction';
            $this->nbgPassword = env('NBG_PASSWORD');
            $this->nbgClient = env('NBG_CLIENT');
        }

        $this->page_set_id = env('NBG_TEST_MODE', true) ? 243 : 3366; // 243 dev // 3366 live
    }

    // start payment
    public function nbgPayment($amount, $purchase_desc, $merchantreference, $lang = 'en')
    {
        $purchase_datetime = Carbon::parse('now')->format('Ymd H:i:s');

        /* create the xml */

        $xml = $this->createXMLRequest($amount, $merchantreference, $purchase_datetime, $purchase_desc, $lang);
        // make the request
        $responsData = $this->makeRequest($xml);

        // check response data for error
        if ($responsData['reason'] != 'ACCEPTED') {
            // something is wrong here ......
            // write log
            //flash()->error(($responsData['reason']));
            return 'Error reason: ' . $responsData['reason'];
        }

        // build the url and make the redirection
        $session_id = $responsData['HpsTxn']['session_id'];
        $nbgUrl = $responsData['HpsTxn']['hps_url'] . '?HPS_SessionID=' . $session_id;
        return \Redirect::away($nbgUrl);
    }

    public function error(Request $request)
    {
        return $request;
        //TODO Remove the ledger item
        flash()->success(__('There was an error with the transaction.'));
        return $this->redirect('');
    }

    public function expire(Request $request)
    {
        return $request;
        //TODO Remove the ledger item
        flash()->success(__('The transaction expired.'));
        return $this->redirect('');
    }

    /**
     * success payment
     */
    public function completed(Request $request)
    {
        if ($request->get('dts_reference') && $request->get('dts_reference') != '') {
            $dts_reference = $request->get('dts_reference');
        } else {
            // return error
        }

        $xml = $this->checkTransaction($dts_reference);
        $checkTransaction = $this->makeRequest($xml);
        $new_datacash_reference = $checkTransaction['HpsTxn']['datacash_reference'];

        $getTransactionData = $this->getTransactionData($new_datacash_reference);
        $transactionData = $this->makeRequest($getTransactionData);

        return $transactionData;
    }

    // request method
    private function makeRequest($xml)
    {
        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->nbgUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);

        //convert the XML result into array
        $data = json_decode(json_encode(simplexml_load_string($data)), true);

        return $data;
    }

    // check transaction by reference id
    public function checkTransaction($datacash_reference, $convertToXml = false)
    {
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                <Request>
                  <Authentication>
                      <password>$this->nbgPassword </password>
                      <client>$this->nbgClient</client>
                  </Authentication>
                  <Transaction>
                    <HistoricTxn>
                      <method>query</method>
                      <reference>$datacash_reference</reference>
                    </HistoricTxn>
                  </Transaction>
                </Request>";

        if ($convertToXml) {
            return $this->toArray($xml);
        }
        return $xml;
    }

    // get transaction data
    public function getTransactionData($new_reference_code, $convertToXml = false)
    {
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
            <Request version='2'>
                <Authentication>
                    <password>$this->nbgPassword</password>                                                         
                    <client>$this->nbgClient</client>                                                                   
                </Authentication>
                <Transaction>
                    <HistoricTxn>
                        <reference>$new_reference_code</reference>
                        <method>query</method>
                    </HistoricTxn>
                </Transaction>
            </Request>";

        if ($convertToXml) {
            return $this->toArray($xml);
        }
        return $xml;
    }

    /**
     * first request xml
     * @param $amount
     * @param $merchantreference
     * @param $purchase_datetime
     * @param $purchase_desc
     * @param $capturemethod
     * @param string $lang
     * @return string
     */
    public function createXMLRequest($amount, $merchantreference, $purchase_datetime, $purchase_desc, $lang = 'en')
    {
        $xml = "<?xml version='1.0' encoding='UTF-8'?>
                   <Request version='2'>
                <Authentication>
                    <password>$this->nbgPassword</password>
                    <client>$this->nbgClient</client>
                </Authentication>
                <Transaction>
                        <TxnDetails>
                            <merchantreference>$merchantreference</merchantreference>
                            <ThreeDSecure>
                                <Browser>
                                    <device_category>0</device_category>
                                    <accept_headers>*/*</accept_headers>
                                    <user_agent>IE/6.0</user_agent>
                                </Browser>
                                <purchase_datetime>$purchase_datetime</purchase_datetime>
                                <merchant_url>https://www.optimalstrom.com</merchant_url>
                                <purchase_desc>$purchase_desc</purchase_desc>
                                <verify>yes</verify>
                            </ThreeDSecure>
                            <capturemethod>ecomm</capturemethod>
                            <amount currency='EUR'>$amount</amount>
                        </TxnDetails>
                        <HpsTxn>
                            <page_set_id>$this->page_set_id</page_set_id>
                            <DynamicData>
                                <dyn_data_2>
                                    <![CDATA[{'lang': '$lang','merchantName': 'optimalstrom','backUrl': '$this->backUrl'}]]>
                                </dyn_data_2>
                            </DynamicData>
                            <method>setup_full</method>
                            <return_url>$this->return_url</return_url>
                            <expiry_url>$this->expiry_url</expiry_url>
                            <error_url>$this->error_url</error_url>
                        </HpsTxn>
                        <CardTxn>
                            <method>auth</method>
                        </CardTxn>
                    </Transaction>
                <UserAgent>
                    <architecture version='2.6.32 - 5 - 686'>i386-Linux</architecture>
                    <language vendor='Sun Microsystems Inc ' version='20.1 - b02' vm-name='Java HotSpot(TM) Server VM'>Java</language>
                    <Libraries><lib version='v2 - 1 - 3'>XMLDocument</lib></Libraries>
                </UserAgent>
            </Request>";

        return $xml;
    }

    public function toArray($xml)
    {
        $xml = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        return json_decode(json_encode($xml), true);
    }
}
