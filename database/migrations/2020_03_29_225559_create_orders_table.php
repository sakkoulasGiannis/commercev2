<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('order_num');
            $table->ipAddress('ip')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('shipping_id')->nullable();
            $table->text('tracking_number')->nullable();
            $table->text('comment')->nullable();
            $table->text('admin_comment_id')->nullable(); // na ftiakso admin_comments  table;;
            $table->string('slug')->nullable(); // create a random slug for the user
            $table->timestamps();
        });

        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id')->unsigned();
            $table->string('status')->default('pending'); // 1 pending 2 proccessing 3 completed 4 canceled
            $table->integer('invoice_id')->nullable(); // na ftiakso invoice table
            $table->string('payment_id'); // na ftiakso payment_method table;
            $table->string('shipping_id');
            $table->float('total')->nullable();
            $table->integer('sub_total')->nullable();
            $table->string('billing_name')->required();
            $table->string('billing_lastname')->required();
            $table->integer('billing_country_id');
            $table->string('billing_region')->required();
            $table->string('billing_city')->required();
            $table->string('billing_street')->required();
            $table->string('billing_zip')->required();
            $table->string('billing_street_number')->required();
            $table->string('billing_phone')->required();
            $table->string('billing_mobile')->required();

            $table->integer('shipping_country_id');
            $table->string('shipping_region')->required();
            $table->string('shipping_city')->required();
            $table->string('shipping_street')->required();
            $table->string('shipping_zip')->required();
            $table->string('shipping_street_number')->required();
            $table->string('shipping_phone')->required();
            $table->string('shipping_mobile')->required();
            $table->string('shipping_name')->required();
            $table->string('shipping_lastname')->required();
            $table->timestamps();
        });

        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id')->unsigned();

            $table->integer('product_id'); // parent producct id
            $table->integer('stock_id');
            $table->string('name');
            $table->string('sku');
            $table->float('price');
            $table->integer('quantity');
            $table->timestamps();
            $table->foreignId('order_detail_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_products');
        Schema::dropIfExists('order_details');
    }
}
