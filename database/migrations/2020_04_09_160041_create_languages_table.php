<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->id();
            $table->string('name')->required();
            $table->string('title')->required();
            $table->string('image')->nullable();
            $table->boolean('default')->default(0);
            $table->boolean('enable')->default(1);
            $table->timestamps();
        });
        $post = new \App\Language();
        $post->title = 'Greek';
        $post->name = 'el';
        $post->default = 1;
        $post->enable = 1;
        $post->save();

        $post = new \App\Language();
        $post->title = 'English';
        $post->name = 'en';
        $post->default = 0;
        $post->enable = 1;
        $post->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
