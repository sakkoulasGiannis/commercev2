<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->integer('discount_type')->default(1); // 1 percent/ 2 value
            $table->float('discount_value')->nullable();

            $table->boolean('executed')->default(0);
            $table->boolean('is_active')->default(1);
            $table->integer("order")->default(0);
            $table->json("query");
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            // You know what this is...
            $table->timestamps();
        });
        Schema::create('rule_variation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->foreignId('rule_id')->constrained()->onDelete('cascade');

            $table->boolean('include')->default(0);

        });
        Schema::create("product_rules", function (Blueprint $table) {

            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->foreignId('rule_id')->constrained()->onDelete('cascade');


            $table->primary(['product_id', 'rule_id']);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_rules');
        Schema::dropIfExists('rule_variation');
        Schema::dropIfExists('rules');
    }
}
