<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($a = 1; $a <= 3; $a++) {
            DB::table('products')->insert([
                'title' => $faker->realText(30),
                'product_type' => 'Dynamic',
                'name' => $faker->slug . rand(11110, 100000010),
                'sku' => $faker->realText(10) . rand(111110, 1000111100),
                'is_active' => 1,
                'tax_class_id' => 1,
                'price' => $faker->randomNumber(2),
                'description' => $faker->paragraph(),
                'group'=>1
            ]);
        }

    }
}
