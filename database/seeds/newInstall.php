<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class newInstall extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->insert(['name' => 'admin', 'guard_name'=>'web']);
        DB::table('roles')->insert(['name' => 'user', 'guard_name'=>'web']);

        $user = DB::table('users')->insert([
            'name' => "Gianni",
            'lastname' => "Sakkoulas",
            'mobile' => "6936780044",
            'phone' => "",
            'email' => "sakkoulas@gmail.com",
            "password" => Hash::make("sakkoulas3@!"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);



        App\User::first()->assignRole(Spatie\Permission\Models\Role::first());
//        $user->assignRole($admin);
    }
}
