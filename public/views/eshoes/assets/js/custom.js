document.addEventListener('DOMContentLoaded', function () {

    var sidenavmenu = document.querySelectorAll('.sidenav.menu');
    var sideNavMenu = M.Sidenav.init(sidenavmenu);


    var sidenavcart = document.querySelectorAll('.sidenav.cart');
    var cartSideBar = M.Sidenav.init(sidenavcart, {edge:'right', size:'350px'});




    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, {
        fullWidth: true,
        indicators: true,
    });


    var slider = document.getElementById('test-slider');
    noUiSlider.create(slider, {
        start: [20, 80],
        connect: true,
        step: 1,
        orientation: 'horizontal', // 'horizontal' or 'vertical'
        range: {
            'min': 0,
            'max': 100
        }

    });


    var megaelems = document.querySelectorAll('.dropdown-trigger');
    var meganav = document.querySelectorAll('.nav-extended');
    var instances = M.Dropdown.init(megaelems, {
        hover: true,
        constrainWidth: false, // full width
        container: meganav,
        coverTrigger: false, // overlap menu
        alignment: 'left'
    });


//    product page
    var tabs = document.querySelectorAll('.tabs');
    var instance = M.Tabs.init(tabs);

    var materialboxed = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(materialboxed);

    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
});

