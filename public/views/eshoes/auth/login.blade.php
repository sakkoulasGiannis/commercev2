@extends('eshoes.layouts.app')
@section('content')
    <div class="section ">
        <div class="row " style="min-height: 400px;" >
            <div class="col s6 offset-s3 ">
                <div class="section">
                    <h5 class="title text-left"><b>Είσοδος</b></h5>

                </div>
                <form class="ps-form--auth" method="POST" action="{{ route('login') }}">
                    @csrf

                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="email">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror


                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <p class="left">
                        <label>
                            <input   type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                            <span>Να με θυμάσαι</span>
                        </label>
                    </p>

                    <button class="right waves-effect waves-light btn">ΕΙΣΟΔΟΣ</button>

                </form>
            </div>
        </div>
    </div>


@endsection
