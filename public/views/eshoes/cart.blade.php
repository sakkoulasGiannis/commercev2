@extends('eshoes.layouts.app')
@section('content')

        <div class="row">
            <div class="col s12">
                @if(Session::has('message'))
                    <p class="alert alert-danger">{{ Session::get('message') }}</p>
                @endif
                <h1 class="small">Καλάθι Αγορών</h1>
               <div class="section">
                   <table class="table table-sm p">
                       <thead>
                       <tr>

                           <th>ΤΙΤΛΟΣ</th>
                           <th>ΤΙΜΗ</th>
                           <th>ΤΕΛΙΚΗ </th>
                           <th>ΤΜΧ</th>
                           <th>ΑΦΑΙΡΕΣΗ</th>
                       </tr>
                       </thead>

                       <tbody>
                       @foreach($items as $val => $item)
                           <tr>
                               <td>{{$item->name}}</td>
                               <td>{{$item->price}}€</td>
                               <td>{{$item->getPriceSum()}}€</td>
                               <td>{{$item->quantity}}</td>
                               <td>
                                   {!! Form::open(['route' => ['cart.deleteItem', $item->id], 'method' => 'post']) !!}
                                   <button class="unstyled trunsparent pointer"><i class="small material-icons">delete_forever</i></button>
                                   {!! Form::close() !!}

                               </td>
                           </tr>
                       @endforeach
                       </tbody>
                       <tfoot>
                       @if(count($conditions)> 0)
                           @foreach($conditions as $condition)
                               <tr>
                                   <td>{{$condition->getType()}}</td>
                                   <td>{{$condition->getName()}}</td>
                                   <td>{{$condition->getValue()}}</td>
                                   <td></td>
                                   <td><a href="{{route('cart.remove.voucher', $condition->getName())}}"><i class="fa fa-trash"></a></td>
                               </tr>
                           @endforeach
                       @endif
                       <tr>
                           <td></td>
                           <td>ΤΕΛΙΚΟ ΣΥΝΟΛΟ</td>
                           <td>{{$cartDetails['total']}}€</td>
                           <td>{{$cartDetails['total_quantity']}}</td>
                           <td></td>
                       </tr>
                       </tfoot>
                   </table>
               </div>
            </div>
            <div class="section">
                <div class="col s12 mt-5 right-align">
                    <a class="btn btn-default" href="{{route('cart.checkout')}}">ΟΛΟΚΛΗΡΩΣΗ  <i class="material-icons right">send</i></a>
                </div>
            </div>


        </div>


@endsection
