@extends('eshoes.layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="breadcrump">
                <div class="col s12">
                    {{--                <a href="#" class="breadcrumb">Αρχική</a>--}}
                    {{--                <a href="#" class="breadcrumb">Γυναίκεία</a>--}}
                    {{--                <a href="#" class="breadcrumb">Τσάντες - Αξεσουάρ</a>--}}
                </div>

            </div>

            <div class="col s2 filters hide-on-med-and-down filters">

                <div id="test-slider"></div>
                <form ref="form" method="GET" action="{{url()->current()}}">

                    <div class="switch">
                        <p>
                            <label>
                                <input type="checkbox" name="discounted" value="true"
                                       {{(request()->has('discounted') &&  request()->get('discounted') == 'true')?'checked':null}}
                                       class="checkbox-orange"/>
                                <span>Μόνο Εκπτωτικά Προϊόντα</span>
                            </label>
                        </p>

                    </div>
                    <hr>

                    <input type="hidden" name="query" value="true">
                    @foreach($fields as $field)
                        <span class="filterHead">{{$field->title}}</span>
                        <div class="filter-block" >
                            @foreach($field->values as $value)
                                @if($value->title !='')
                                    <p>
                                        <label>
                                            <input type="checkbox" name="{{$field->name}}" value="{{$value->name}}" {{(request()->has($field->name) && $value->name == request()->get($field->name))?'checked ':null}}class="checkbox-orange"/>
                                            <span>{{$value->title}}</span>
                                            {{--                                            <span>{{$value->title}}</span>--}}
                                        </label>
                                    </p>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                    <button>Search</button>
                </form>
            </div>
            <div class="col s12 m10">
                <div class="row productsListing">
                    <div class="col s12 categoryHeader">
                        <div class="row ">
                            <!--                        <a href="#" data-target="filter-nav" class="sidenav-trigger hide-on-large-only"><i-->
                            <!--                                class="material-icons">filter_list</i>ΦΙΛΤΡΑ</a>-->
                            <div class="col s3 m3">
                                <form  method="GET">
                                    <select onchange="this.form.submit()" class="browser-default left" name="sort">
                                        <option value="" disabled selected>Νέα Προϊόντα</option>
                                        <option value="price_asc">Τιμή Αύξουσα</option>
                                        <option value="price_desc">Τιμή Φθίνουσα </option>
                                        <option value="discount_asc">Ποσοστό Έκτπωσης Αύξουσα</option>
                                        <option value="discount_desc">Ποσοστό Έκπτωσης Φθίνουσα</option>
                                        <option value="season_asc">Νέες Παραλαβές Αύξουσα</option>
                                        <option value="season_desc">Νέες Παραλαβές Φθίνουσα</option>
                                    </select>
                                </form>
                            </div>

                            <div class="col s2 m1 left">

                                <select class="browser-default">
                                    <option value="" disabled selected>24</option>
                                    <option value="1">48</option>
                                    <option value="2">90</option>
                                    <option value="3">120</option>
                                </select>
                            </div>

                            <div class="col s7 m8 right-align">
                                {{$products->links('eshoes.pagination')}}
                            </div>
                        </div>
                    </div>
                    @foreach($products as $product)

                        @include('eshoes.elements.productListingItem', $product)
                    @endforeach

                </div>

                <div class="col s12  center-align">
                    {{$products->links('eshoes.pagination')}}
                </div>
            </div>
        </div>
    </div>
@endsection
