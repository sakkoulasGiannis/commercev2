@extends('eshoes.layouts.app')
@section('content')
    <div class="section">
        <div class="row checkout">
            <h1 class="small">Ολοκλήρωση Αγορών</h1>
            {!! Form::open(['route' => 'cart.checkout.complete', 'method' => 'post']) !!}
            @csrf
            <div class="col s12 m4">
                <div class="">
                    {{-- User name --}}
                    <h2>ΔΙΕΥΘΥΝΣΗ ΧΡΕΩΣΗΣ</h2>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="billing_name" name='billing_name' type="text" class="validate">
                            <label for="billing_name">Όνομα</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="billing_lastname" name="billing_lastname" type="text" class="validate">
                            <label for="billing_lastname">Επίθετο</label>
                        </div>
                    </div>
                    {{--  user email --}}
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="email" type="email" name="email" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    {{--  user address --}}
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="billing_street"  name="billing_street" type="text" class="validate">
                            <label for="billing_street">Διεύθυνση</label>
                        </div>
                    </div>
                    {{--  user city --}}
                    <div class="row">
                        <div class="input-field col s6">
                            <input  id="billing_city" name="billing_city" type="text" class="validate">
                            <label for="billing_city">Πόλη</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="billing_region" name="billing_region" type="text" class="validate">
                            <label for="billing_region">Νομός/Δήμος</label>
                        </div>
                    </div>
                    {{--  user city --}}
                    <div class="row">
                        <div class="input-field col s6">
                            <input  id="billing_zip" name="billing_zip" type="text" class="validate">
                            <label for="billing_zip">Τ.Κ</label>
                        </div>
                        <div class="input-field col s6">
                            <select name="billing_country_id" required>
                                <option value="1" selected>Greece</option>
                                <option value="2">Cyprus</option>
                            </select>
                            <label>Επιλογή Χώρας</label>
                        </div>


                    </div>
                    {{--  user address --}}
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="billing_mobile" name="billing_mobile" type="text" class="validate">
                            <label for="billing_mobile">Τηλέφωνο</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m8">


                <div class="row">
                    <div class="col s12 m6 section">
                        <h2>ΜΕΘΟΔΟΣ ΑΠΟΣΤΟΛΗΣ</h2>
                        <p>
                            <label>
                                <input name="shipping_id" value="acs" type="radio" required />
                                <span>Acs Courier</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="shipping_id" value="elta" type="radio" />
                                <span>ΕΛΤΑ Courier</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="shipping_id" value="geniki" type="radio" />
                                <span>Γενική Ταχυδρομική</span>
                            </label>
                        </p>
                    </div>
                    <div class="col s12 m6 section">
                        <h2>ΜΕΘΟΔΟΣ ΠΛΗΡΩΜΗΣ</h2>
                        <p>
                            <label>
                                <input name="payment_id" value="creditcart" type="radio" required />
                                <span>Πληρωμή με πιστωτική/χρεωστική κάρτα</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="payment_id" value="cod" type="radio" checked />
                                <span>Αντικαταβολή</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="payment_id" value="paypal" type="radio" />
                                <span>Πληρωμή με Paypal</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="payment_id" value="bank" type="radio" />
                                <span>Κατάθεση σε λογαριασμό Τραπέζης</span>
                            </label>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="section">
                        <div class="col s12 m12">
                            <h2>ΑΝΑΣΚΟΠΗΣΗ ΠΑΡΑΓΓΕΛΙΑΣ</h2>
                            <table class="table table-sm p">
                                <thead>
                                <tr>

                                    <th>ΤΙΤΛΟΣ</th>
                                    <th>ΤΙΜΗ</th>
                                    <th>ΤΕΛΙΚΗ </th>
                                    <th>ΤΜΧ</th>

                                </tr>
                                </thead>

                                <tbody>
                                <?php $cartContent = \Cart::getContent();?>
                                @foreach($cartContent as $val => $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->price}}€</td>
                                        <td>{{$item->getPriceSum()}}€</td>
                                        <td>{{$item->quantity}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                @if(count($conditions)> 0)
                                    @foreach($conditions as $condition)
                                        <tr>
                                            <td>{{$condition->getType()}}</td>
                                            <td>{{$condition->getName()}}</td>
                                            <td>{{$condition->getValue()}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td></td>
                                    <td>ΤΕΛΙΚΟ ΣΥΝΟΛΟ</td>
                                    <td>{{$cartDetails['total']}}€</td>
                                    <td>{{$cartDetails['total_quantity']}}</td>
                                    <td></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <button>Apply</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('page_scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var select = document.querySelectorAll('select');
            var instances = M.FormSelect.init(select);
        });
    </script>
@endsection
