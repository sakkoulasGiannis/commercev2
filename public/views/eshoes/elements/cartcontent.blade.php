

<ul id="cartcontent" class="sidenav cart ">
    <li>
        <p class="title">Καλάθι Αγορών</p>
    </li>

    <li>

        <?php $cartContent = \Cart::getContent();?>
        @if(count($cartContent)>0)
            <ul class="collection">
            @foreach($cartContent as $val => $item)
                <li class="collection-item avatar">

                    <img src="{{$item->attributes['img_url']}}" class="circle"  >
                    <span class="title"><b>{{$item->name}}</b></span>
                    <div class="right">
                        {!! Form::open(['route' => ['cart.deleteItem', $item->id], 'method' => 'post']) !!}
                        <button class="unstyled trunsparent pointer"><i class="tiny material-icons">delete_forever</i></button>
                        {!! Form::close() !!}
                    </div>
                    <div class="sideCartDetails">
                        <span> <b>Τιμή:</b> {{$item->price}}€</span>
                        <span> <b>Τμχ:</b> {{$item->quantity}}</span>
                        <span> <b>Συνόλο:</b> {{$item->getPriceSum()}}€</span>
                    </div>
                </li>
            @endforeach
            </ul>
        @else
            <span>Δεν έχετε προϊόντα στο καλάθι</span>
        @endif
    </li>
    @if(count($cartContent)>0)
        <li class="center"><a class="waves-effect teal white-text" href="/cart/checkout"> Συνέχεια στο Καλάθι</a></li>
        <li class="center"><br/><a class="" href="#!">Συνέχεια αγορών</a></li>
    @endif
</ul>
