<div class="col s6 m4 l3 productItem" >
    @if($product->rules)
        <div class="ribon">
            -{{$product->rules->discount_value}}%
        </div>
    @endif


    @if($product->getMedia()->first())

        <a href="/{{$product->name}}" class="">
            @if(count($product->getMedia()) > 0)

                <img class="img-fluid" src=" {{$product->getMedia()->first()->getUrl('thumb')}}">
            @else
                 <img class="img-fluid" src="https://softsmart.co.za/wp-content/uploads/2018/06/image-not-found-1038x576.jpg">
            @endif
        </a>

    @else
            <a href="/{{$product->name}}" class="noimage" >
                <img class="img-fluid" src="https://via.placeholder.com/247">
            </a>
    @endif

    <div class="productListingDetails ">

        <div class="productCode left-align  ">
            <a href="/{{$product->name}}" class="grey-text" title="">#{{$product->sku}}</a>

            <span class="right">
                @if($product->originalPrice)

                    <del class="small text">{{$product->originalPrice}} €</del>
                @endif
            </span>
        </div>

        <div class="center-align ">
            <div class="productTitle left">
                @if($product->brand)
                    <a href="/{{$product->name}}"><b>{{$product->brand->first()->title}}</b></a>
                @endif

            </div>
            <div class="right ">
                <span class="productListingPrice">{{$product->price}} €</span>

            </div>
        </div>
    </div>
</div>
