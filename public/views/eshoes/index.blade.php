@extends('eshoes.layouts.app')
@section('content')
    <div class="row">
        <div class="col s12">
            <div class="carousel carousel-slider ">
                <a class="carousel-item" href="#one!"><img src="{{asset('views/eshoes/assets/img/top.jpg')}}"></a>

            </div>
        </div>
    </div>

   <div class="container">
       <div class="row">
           <div class="col s6 m6 mb2">

               <figure class="snip1104 " style="max-height: 548px;">
                   <img src="{{asset('views/eshoes/assets/img/left.jpg')}}" alt="sample33" />
                   <figcaption>
                       <h2>ΓΥΝΑΙΚΕΙΑ  <span> ΠΑΠΟΥΤΣΙΑ</span></h2>
                   </figcaption>
                   <a href="#"></a>
               </figure>
           </div>

           <div class="col s6 m3">

               <figure class="snip1104 red" style="max-height: 255px;">
                   <img src="{{asset('views/eshoes/assets/img/middle-bottom.jpg')}}" alt="sample33" />
                   <figcaption>
                       <h2>ΤΣΑΝΤΕΣ</h2>
                   </figcaption>
                   <a href="#"></a>
               </figure>
               <br>
               <figure class="snip1104 red" style="max-height: 250px;">
                   <img src="{{asset('views/eshoes/assets/img/middle-top.jpg')}}" alt="sample33" />
                   <figcaption>
                       <h2>ΠΑΙΔΙΚΑ  <span> ΠΑΠΟΥΤΣΙΑ</span></h2>
                   </figcaption>
                   <a href="#"></a>
               </figure>
           </div>
           <div class="col s6 m3">
               <figure class="snip1104 " style="max-height: 548px;">
                   <img src="{{asset('views/eshoes/assets/img/right.jpg')}}" alt="sample33" />
                   <figcaption>
                       <h2>ΑΝΔΡΙΚΑ  <span> ΠΑΠΟΥΤΣΙΑ</span></h2>
                   </figcaption>
                   <a href="#"></a>
               </figure>
           </div>
       </div>
       <div class="row">
           <div class="section">
               <div class="col s12">
            <span class="title header-1">
                ΔΗΜΟΦΙΛΗ ΓΥΝΑΙΚΕΙΑ
            </span>
               </div>
               @foreach($featureProducts as $product)
                   @include('eshoes.elements.productListingItem', $product)
               @endforeach

           </div>

       </div>

   </div>


@endsection
