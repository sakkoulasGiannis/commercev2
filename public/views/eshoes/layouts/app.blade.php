<!DOCTYPE html>
<html lang="el">
<head>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K63M6ZC');</script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800|Roboto:400,700,900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="{{asset('views/eshoes/assets/css/style.css')}}">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NN9WMHB');</script>
    <!-- End Google Tag Manager -->
</head>


<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K63M6ZC"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NN9WMHB"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app">
    <div class="container mt-5 mb-5 pt-5">
        <!--  preheader  -->
    {{--        <div class="row preheader">--}}
    {{--            <div class="col s5 m4 icons">--}}
    {{--                <a href="#"><i class="material-icons">face</i></a>--}}
    {{--                <a href="#"><i class="material-icons">favorite</i></a>--}}
    {{--                <a href="https://www.facebook.com/e.shoes.online"><i class="fab fa-facebook-f"></i></a>--}}
    {{--                <a href="https://www.instagram.com/eshoes.gr/"><i class="fab fa-instagram"></i></a>--}}
    {{--            </div>--}}
    {{--            <div class="col s6 m4">--}}
    {{--                <form class="" method="post" action="">--}}
    {{--                    <div id="search">--}}
    {{--                        <form action="" id="searchForm">--}}

    {{--                            <input id="" type="text" class="txt-rotate" data-period="2000"--}}
    {{--                                   data-rotate='[ "Μαύρες γόβες.", "Κόκκινα Sneakers", "Tammaris" ]'--}}
    {{--                                   placeholder="search">--}}
    {{--                            <button><span class="icon"><i class="fas fa-search"></i></span></button>--}}
    {{--                        </form>--}}
    {{--                    </div>--}}
    {{--                </form>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    <!--  header  -->
        <div class="row">
            {{--  message --}}
            <div class="col s12 m3 hide-on-small-and-down">
                {{--                <span class="orange-text headertext">ΔΩΡΕΑΝ</span>--}}
                {{--                <span class="shipping headertext">ΑΠΟΣΤΟΛΗ & ΕΠΙΣΤΡΟΦΗ</span>--}}
                {{--                <span class="ordercost headertext">Για οποιοδήποτε κόστος παραγγελίας</span>--}}
                <div class="section">
                    <form class="pt20" method="post" action="">
                        <div id="search">
                            <form method='get' action="/search" id="searchForm">
                                <input id="" type="text" class="txt-rotate" data-period="2000"
                                       data-rotate='[ "Μαύρες γόβες.", "Κόκκινα Sneakers", "Tammaris" ]'
                                       placeholder="search">
                                <button><span class="icon"><i class="fas fa-search"></i></span></button>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
            {{--  logo --}}
            <div class="col s6 m4 offset-m1 push-s3">
                <div class="section">
                    <div class="logo">
                        <a href="/" title="home page">
                            <img src="https://www.e-shoes.gr/skin/frontend/base/default/images/menoume-asfaleis-eshoes-logo.jpg" alt="logo">
{{--                            <img src="{{asset('views/eshoes/assets/img/e-shoes-logo-samples.png')}}" alt="logo">--}}
                        </a>
                    </div>
                </div>
            </div>
            {{--  Cart --}}
            <div class="col s3 m4 push-s3">
                <div class="section pt20">

                    <div class="right-align relative valign-wrapper right">

                        <a href="#" class="sidenav-trigger pt10 pr20"><i class="material-icons">face</i></a>
                        <a href="#" class="sidenav-trigger  pt10 pr20"><i class="material-icons ">favorite</i></a>
                        <?php $cartContent = \Cart::getContent();?>
                        <a href="#" data-target="cartcontent" class="sidenav-trigger right">
                            {{--                        <span class="to-kalathi-mou hide-on-small-and-down">ΤΟ ΚΑΛΑΘΙ ΜΟΥ</span>--}}
                            <img class="cartIcon" style="width: 50px;"
                                 src="{{asset('views/eshoes/assets/img/kalathi-icon.png')}}" alt="">
                            <span class="itemsInCart">{{$cartContent->count()}}</span>
                        </a>

                    </div>
                </div>
            </div>
            {{--nav --}}

            <div class="col s3 m12 pull-s9">
                <nav id="mainnav" class="z-depth-0 nav-extended">
                    <div class="nav-wrapper ">
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                        <ul class="left hide-on-med-and-down navliks">
                            @foreach($allCategories as $parent)
                                <li>
                                    <a class="blue-text text-darken-4 {{(count($parent->children) > 0)? 'dropdown-trigger':null}}"
                                       {{($parent->mega_menu_is_active )? "data-target=dropdown{$parent->id}":null}} href="/{{$parent->name}}">
                                        {{$parent->title}}

                                    </a>
                                    <div class="container dropdown-content" id="dropdown{{$parent->id}}">
                                        {!! $parent->mega_menu_content !!}
                                    </div>
                                    {{--                                       {{(count($parent->children) > 0)? "data-target=dropdown{$parent->id}":null}} href="/{{$parent->name}}">{{$parent->title}}</a>--}}
                                    {{--                                    @if(count($parent->children) > 0)--}}
                                    {{--                                        <div class="container dropdown-content" id="dropdown{{$parent->id}}">--}}
                                    {{--                                            <div class="row">--}}
                                    {{--                                                @foreach($parent->children as $child)--}}
                                    {{--                                                    <div class="col">--}}
                                    {{--                                                        <ul>--}}
                                    {{--                                                            <li class="title">--}}
                                    {{--                                                                <a class="level1" href="/{{$parent->name}}/{{$child->name}}"><span>{{$child->title}}</span></a>--}}
                                    {{--                                                            </li>--}}
                                    {{--                                                            @if(count($child->children)>0)--}}
                                    {{--                                                                @foreach($child->children as $subchilds)--}}
                                    {{--                                                                    <li class="menu-item " style="list-style: none;">--}}
                                    {{--                                                                        <a class="level2" href="/{{$parent->name}}/{{$child->name}}/{{$subchilds->name}}"><span>{{$subchilds->title}}</span></a>--}}
                                    {{--                                                                    </li>--}}
                                    {{--                                                                @endforeach--}}
                                    {{--                                                            @endif--}}
                                    {{--                                                        </ul>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                @endforeach--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    @endif--}}

                                </li>
                            @endforeach

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>

    <div class="section darkBlue p-0">
        <div class="row">
            <div class="text-center center">
                <p class="white-text">ΔΩΡΕΑΝ ΑΠΟΣΤΟΛΗ ΚΑΙ ΕΠΙΣΤΡΟΦΗ ΣΕ ΟΛΗ ΤΗΝ ΕΛΛΑΔΑ.</p>
            </div>
        </div>
    </div>

        <!--  nav  -->
        <ul class="sidenav menu" id="mobile-demo">
            @foreach($allCategories as $parent)
                <li>
                    <a class="blue-text text-darken-4 {{(count($parent->children) > 0)? 'dropdown-trigger':null}}"
                       {{($parent->mega_menu_is_active )? "data-target=dropdown{$parent->id}":null}} href="/{{$parent->name}}">
                        {{$parent->title}}

                    </a>
                    <div class="container dropdown-content" id="dropdown{{$parent->id}}">
                        {!! $parent->mega_menu_content !!}
                    </div>
                    {{--                                       {{(count($parent->children) > 0)? "data-target=dropdown{$parent->id}":null}} href="/{{$parent->name}}">{{$parent->title}}</a>--}}
                    {{--                                    @if(count($parent->children) > 0)--}}
                    {{--                                        <div class="container dropdown-content" id="dropdown{{$parent->id}}">--}}
                    {{--                                            <div class="row">--}}
                    {{--                                                @foreach($parent->children as $child)--}}
                    {{--                                                    <div class="col">--}}
                    {{--                                                        <ul>--}}
                    {{--                                                            <li class="title">--}}
                    {{--                                                                <a class="level1" href="/{{$parent->name}}/{{$child->name}}"><span>{{$child->title}}</span></a>--}}
                    {{--                                                            </li>--}}
                    {{--                                                            @if(count($child->children)>0)--}}
                    {{--                                                                @foreach($child->children as $subchilds)--}}
                    {{--                                                                    <li class="menu-item " style="list-style: none;">--}}
                    {{--                                                                        <a class="level2" href="/{{$parent->name}}/{{$child->name}}/{{$subchilds->name}}"><span>{{$subchilds->title}}</span></a>--}}
                    {{--                                                                    </li>--}}
                    {{--                                                                @endforeach--}}
                    {{--                                                            @endif--}}
                    {{--                                                        </ul>--}}
                    {{--                                                    </div>--}}
                    {{--                                                @endforeach--}}
                    {{--                                            </div>--}}
                    {{--                                        </div>--}}
                    {{--                                    @endif--}}

                </li>
            @endforeach

        </ul>

        @include('eshoes.elements.cartcontent')
        @yield('content')

</div>

<div class="container">
    <div class="row">
        <div class="col m12">
            <div class="row">
                <div class="col m3">
                    <div class="panel darkBlue white-text text-center callus">
                        <i class="fas fa-phone"></i> 28102228000
                    </div>
                </div>

                <div class="col m3">
                    <div class="panel darkBlue white-text text-center free shipping">
                        <span class="freeShippingTet">ΔΩΡΕΑΝ</span> <br/>
                        Αποστολή & Επιστροφή
                    </div>
                </div>
                <div class="col m3">
                    <div class="panel darkBlue white-text text-center cyprusTextShipping">
                        <span class="cyprusText">ΑΠΟΣΤΟΛΕΣ ΣΤΗΝ ΚΥΠΡΟ</span><br/>
                        Δωρεάν μεταφορικά για αγορές άνω των 70€
                    </div>
                </div>
                <div class="col m3">
                    <div class="panel darkBlue white-text text-center paymentMethodText">
                        <span class="paymentTextHeader">ΠΛΥΡΩΜΗ ΜΕ ΑΝΤΙΚΑΤΑΒΟΛΗ</span><br/>
                        ή e-banking
                    </div>
                </div>
            </div>
            <div class="  flex">
                <div class="col m5 lightblue  ">
                    <h5 class="white-text">Εγγραφή στο Newssleter</h5>
                    <input type="text" >
                    <a class="right waves-effect waves-light btn darkBlue white-text">ΕΓΓΡΑΦΗ</a>

                    <script>if (!window.mootrack) {
                            !function (t, n, e, o, a) {
                                function d(t) {
                                    var n = ~~(Date.now() / 3e5), o = document.createElement(e);
                                    o.async = !0, o.src = t + "?ts=" + n;
                                    var a = document.getElementsByTagName(e)[0];
                                    a.parentNode.insertBefore(o, a)
                                }

                                t.MooTrackerObject = a, t[a] = t[a] || function () {
                                    return t[a].q ? void t[a].q.push(arguments) : void (t[a].q = [arguments])
                                }, window.attachEvent ? window.attachEvent("onload", d.bind(this, o)) : window.addEventListener("load", d.bind(this, o), !1)
                            }(window, document, "script", "https://cdn.stat-track.com/statics/moosend-tracking.min.js", "mootrack");
                        }
                        mootrack('loadForm', 'e65d7474aa8a4a24b24b81438a254394');</script>
                </div>
                <div class="col m7  grey lighten-3 white-text">
                    <div class="row">
                        <div class="col m6 p-5">
                            <div class="section darkblue-text">
                                follow us <a href="#"><i class="fab fa-facebook fa-4x"></i></a> <a href="#"><i
                                        class="fab fa-instagram fa-4x"></i></a>
                            </div>

                        </div>
                        <div class="col m6 right-align ">
                            <div class="section">

                                <img src="https://trustmark.gr/badge/img/badges/trustmark_color_GR.png" alt=""
                                     style="width:100px">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col m4">
                            <h5 class="darkblue-text">Πληροφορίες</h5>
                            <ul>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Ο Λογαριασμός μου </a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Προφίλ Εταιρίας</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Ασφάλειας Συναλλαγών</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Επιοκοινωνήστε μαζί μας</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Προστασία Δεδομένων</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Χάρτης Ιστοχώρου</a></li>
                            </ul>
                        </div>
                        <div class="col m4">
                            <h5 class="darkblue-text">Παραγγελίες</h5>
                            <ul>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Τρόποι Πληρωμής</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Αποστολή Προϊόντων</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Αλλαγές / Επιστροφές</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Συχνές Ερωτήσεις</a></li>
                                <li><a class="darkblue-text text-lighten-3" href="#!">Όροι Χρήσης</a></li>
                            </ul>
                        </div>
                        <div class="col m4">

                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col s12">
                    <img src="https://www.e-shoes.gr/media/footer/banks-tresa.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


{{--<footer class="page-footer">--}}
{{--    <div class="footer-copyright">--}}
{{--        <div class="container">--}}
{{--            © 2019 Copyright e-shoes.gr--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</footer>--}}

<!--mobile nav-->
<div class="sidenav " id="filter-nav">
    <span class="categoryHeader">Μόνο Εκπτωτικά Προϊόντα</span>
    <div class="switch">

        <label>
            ΟΧΙ
            <input type="checkbox">
            <span class="lever"></span>
            ΝΑΙ
        </label>
    </div>

    <hr>

    <div id="test-slider"></div>
    <span class="categoryHeader">Brands</span>

    <p>
        <label>
            <input type="checkbox" class="checkbox-orange"/>
            <span>DKNY</span>
        </label>
    </p>
    <p>
        <label>
            <input type="checkbox"/>
            <span>HOGAN</span>
        </label>
    </p>
    <p>
        <label>
            <input type="checkbox"/>
            <span>MARRON</span>
        </label>
    </p>
    <p>
        <label>
            <input type="checkbox"/>
            <span>TRUSSARDI</span>
        </label>
    </p>

</div>

<!--JavaScript at end of body for optimized loading-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="{{asset('/views/eshoes/assets/js/nouislider.js')}}"></script>
<script src="{{asset('/views/eshoes/assets/js/typingcarousel.js')}}"></script>
<script src="{{asset('/views/eshoes/assets/js/custom.js')}}"></script>

@yield('page_scripts')


</body>
</html>
