@extends('eshoes.layouts.app')
@section('content')

    <div class="row">
        <div class="col m6 sm12">
            @if($product->getMedia() && count($product->getMedia()))
                <div class="carousel carousel-slider">
                    @foreach($product->getMedia() as $var => $image)
                        <a class="carousel-item" href="#{{$var}}!"><img src="{{$image->getUrl()}}"
                                                                        alt="{{$image->getUrl()}}"></a>
                                                <img class="materialboxed" src="{{$image->getUrl()}}" alt="">
                    @endforeach
                </div>
            @endif
                <div class="col s12">
                    <h3>ΠΕΡΙΓΡΑΦΗ</h3>
                    {!! $product->description !!}
                </div>
        </div>
        <div class="col m6">
            <div class="row">
                <div class="col s12">
                    <h1 class="innerProductTitle">{{$product->title}}</h1>
                    <span class="sku"> Κωδικός: {{$product->sku}}</span>

                    <div class="innerProductPrice">
                        {{$product->price}}€
                        @if($product->originalPrice)
                            <del style="color: grey; font-size: 22px;"> {{$product->originalPrice}}€</del>
                        @endif

                    </div>
                    @if($product->brand->first())
                        <div class="brandImage  right">

                            <a href="/brands/{{$product->brand->first()->name}}">{{$product->brand->first()->title}}</a>
                            {{--                <img src="https://www.e-shoes.gr/media/aitmanufacturers/logo/xti.jpg" alt="">--}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="card white z-depth-0">
                        {!! Form::open(['route' => ['cart.add', $product->slug], 'method' => 'post', "class"=> "row"]) !!}
                        {!! Form::hidden('quantity', 1, ['class' => 'form-control']) !!}
                        {!! Form::hidden('type', $product->product_type, ['class' => 'form-control']) !!}
                        <div class="card-content">
                            <div class="productAttributes">
                                *Χρώμα: {{$product->color->first()->title}}
                            </div>
                            <a class='pt-5' href="#" class="selected ">
                                <img class="current pt-2" width="45px" style="width: 55px;"
                                     src="{{$product->getFirstMediaUrl()}}" alt="">
                            </a>
                            @foreach($product->getRelative() as $relative)
                                @if($relative->getFirstMedia())
                                    <a href="{{$relative->name}}">
                                        <img width="45px" style="width: 55px;"
                                             src="{{$relative->getFirstMediaUrl()}}" alt="">
                                    </a>
                                @endif
                            @endforeach
                            <div class="sizeAttribute">
                                *Μέγεθος:
                                <div class="sizes">
                                    @foreach($apothiki as $inv)
                                        @if($inv->id)
                                            @foreach($inv->variations as $var)
                                                @if($inv->quantity > 0)

                                                    @php
                                                        $model = $var['model_type'];
                                                        $size = $model::find($var['model_id'])->title;
                                                    @endphp
                                                    <label class="chip">
                                                        <input type="radio" name="stock" class="filled-in"
                                                               value="{{$inv->id}}" required/>
                                                        <span>{{$size}}</span>
                                                    </label>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn">ΠΡΟΣΘΗΚΗ ΣΤΟ ΚΑΛΑΘΙ</button>
                        </div>
                        {!! Form::close() !!}
                    </div>


                    <div class="col  s12">
                        <ul class="tabs ">
                            <li class="tab col s4"><a href="#test2">Χαρακτηριστικά</a></li>
                            <li class="tab col s4"><a href="#test3">Αποστολές</a></li>
                            <li class="tab col s4"><a href="#test4">Επιστροφές</a></li>
                        </ul>
                    </div>
                    <div class="col s12">
                        <div class="tabs-content">
                            <div id="test2" class="col m12 s12">
                                <table class="data-table" id="product-attribute-specs-table">
                                    <colgroup>
                                        <col width="33%">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    @foreach($fields as $field)
                                        @php
                                            $name = $field->name;
                                            $value = $product->$name()->pluck('title')->first();
                                        @endphp
                                        <tr class="first odd">
                                            <th class="label">{{$field->title}}</th>
                                            <td class="data last">{!! $value !!}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="even">
                                        <th class="label">Είδος</th>
                                        <td class="data last">Βραδινά - Dress, Χωρίς κορδόνι</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="test3" class="col m12 s12">
                                <p><b>Αποστολή Προϊόντων</b></p>
                                <p>Οι αποστολές εντός Ελλάδος ΔΕΝ επιβαρύνονται με έξοδα αποστολής, ανεξαρτήτως αξίας
                                    της
                                    παραγγελίας σας.</p>
                                <p>Η εταιρεία ταχυμεταφορών με την οποία συνεργαζόμαστε είναι η Γενική Ταχυδρομική και η
                                    ACS
                                    COURRIER. Στην περίπτωση που η περιοχή που μένετε δεν εξυπηρετείται από το δίκτυο
                                    των
                                    συνεργατών μας, έχουμε την ευθύνη να σας αποστείλουμε την παραγγελία σας με
                                    εναλλακτικό
                                    τρόπο, πάλι με δωρεάν έξοδα μεταφορικών για εσάς.</p>
                                <p>Πότε θα σας παραδώσουμε; <br>Όλες οι παραγγελίες αποστέλλονται από την εταιρεία μας
                                    την
                                    επόμενη εργάσιμη ημέρα, από την ημέρα που κάνατε την παραγγελία σας. <br>Εφόσον
                                    έχετε
                                    εγγραφεί ως χρήστης στο ηλεκτρονικό μας κατάστημα, μπορείτε ανά πάσα στιγμή να δείτε
                                    την
                                    κατάσταση της παραγγελίας σας πατώντας στο σύνδεσμο «Ο Λογαριασμός μου», που
                                    βρίσκεται
                                    στο πάνω μέρος της σελίδας. <br>Μετά την αποστολή της παραγγελίας σας, έχετε την
                                    δυνατότητα να ενημερώνεστε για την πορεία του δέματος σας, μέσω του ειδικού
                                    συνδέσμου ο
                                    οποίος θα σας σταλεί με e-mail.</p>
                            </div>
                            <div id="test4" class="col m12 s12">
                                <p><b>Αλλαγές - Επιστροφές Προϊόντων</b></p>
                                <p>Έχετε το δικαίωμα να επιστρέψετε το προϊόν που παραλάβατε εντός 30 ημερολογιακών
                                    ημερών
                                    χωρίς κανένα απολύτως κόστος και να ζητήσετε την αντικατάστασή του με άλλο μέγεθος ή
                                    άλλο προϊόν.<br>Για επιστροφές εντός Ελλάδας δεν επιβαρύνεστε με έξοδα μεταφορικών,
                                    αρκεί να χρησιμοποιήσετε την μεταφορική εταιρεία που θα σας υποδείξουμε.</p>
                                <p><b>Ακύρωση Παραγγελίας</b></p>
                                <p>Είναι δυνατόν να ακυρώσετε την παραγγελία σας πριν από την παραλαβή της από την
                                    εταιρεία
                                    ταχυμεταφορών.<br>Για ενδεχόμενη ακύρωση ενημερώστε μας μέσω e-mail στην ηλεκτρονική
                                    διεύθυνση info@e-shoes.gr ή τηλεφωνικά στο 211 800 5292, 09:00-15:00, Δευτέρα έως
                                    Παρασκευή.</p>
                                <p><b>Δικαίωμα Υπαναχώρησης – Επιστροφή Χρημάτων</b></p>
                                <p>Εάν για οποιοδήποτε λόγο επιθυμείτε να επιστρέψετε το προϊόν που αγοράσατε και δε σας
                                    ενδιαφέρει η αντικατάστασή του με κάποιο άλλο μπορείτε να ασκήσετε το δικαίωμά σας
                                    για
                                    υπαναχώρηση κάνοντας αίτημα επιστροφής (μέσω e-mail, με το ονοματεπώνυμο σας και τον
                                    αριθμό παραγγελίας σας ή τηλεφωνικά). Η επιστροφή πρέπει να γίνει εντός 14
                                    ημερολογιακών
                                    ημερών από την ημερομηνία παραλαβής του προϊόντος με μοναδική επιβάρυνση τη δαπάνη
                                    επιστροφής του προϊόντος.</p>
                                <p>Βασική προϋπόθεση για να προχωρήσετε στην επιστροφή του προϊόντος, είναι αυτό να
                                    βρίσκεται στην άριστη κατάσταση που παραλήφθηκε χωρίς φθορές και να είναι πλήρες,
                                    εντός
                                    της αρχικής συσκευασίας του. Όλα τα υποδήματα πρέπει να δοκιμάζονται σε τάπητα για
                                    την
                                    αποφυγή φθορών.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

        @if(Session::has('modal') && Session::get('modal') == true)
        @section('page_scripts')
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    // var elems = document.querySelectorAll('.modal');
                    // var instances = M.Modal.init(elems, {opacity:0.1});
                    // instances[0].open();
                    sidenavcart = document.querySelectorAll('.sidenav.cart');
                    var cartSideBar = M.Sidenav.init(sidenavcart, {edge: 'right'});
                    cartSideBar[0].open();
                });


            </script>
@endsection

@endif
{{--<div id="modal1" class="modal bottom-sheet">--}}
{{--    <div class="modal-content">--}}
{{--        <div class="row">--}}
{{--            <div class="col s12">--}}
{{--                <h4></h4>--}}
{{--                <p>Το προϊόν προστέθηκε στο καλάθι:--}}
{{--                </p>--}}

{{--                <a href="#!" class="teal white-text btn modal-close waves-effect waves-green btn-flat">Συνέχεια αγορών</a>--}}
{{--                <a href="/cart/checkout" class="right green white-text  btn modal-close waves-effect waves-green btn-flat">ΟΛΟΚΛΗΡΩΣΗ ΑΓΟΡΑΣ</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--</div>--}}


@endsection
@section('page_scripts')


@endsection


