@extends('eshoes.layouts.app')
@section('content')
    <div class="section min-height" >
        <div class="row min-vh-100">
            <div class="col s12">
                <span class="title"><b>ΕΥΧΑΡΙΣΤΟΥΜΕ ΓΙΑ ΤΗΝ ΠΑΡΑΓΓΕΛΙΑ ΣΑΣ!</b></span>
                <p>
                    Ο κωδικός παραγγελίας είναι: {{session('order_id')}}
                </p>
                <p>Για την επιβεβαίωση της παραγγελίας σας, θα λάβετε σχετικό e-mail.</p>
            </div>
        </div>
    </div>
@endsection
@section('page_scripts')

@endsection
