/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vuetify from 'vuetify';
import { TiptapVuetifyPlugin } from 'tiptap-vuetify'
// don't forget to import CSS styles
import 'tiptap-vuetify/dist/main.css'
// Vuetify's CSS styles
import 'vuetify/dist/vuetify.min.css'


window.Vue = require('vue');
 // Vue.use(TiptapVuetifyPlugin);
// Vue.use(Vuetify);

const vuetify = new Vuetify()

// use Vuetify's plugin
Vue.use(Vuetify)
// use this package's plugin
Vue.use(TiptapVuetifyPlugin, {
    // the next line is important! You need to provide the Vuetify Object to this place.
    vuetify, // same as "vuetify: vuetify"
    // optional, default to 'md' (default vuetify icons before v2.0.0)
    iconsGroup: 'md'
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scdan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dashboard', require('./components/manage/dashboard.vue').default);
Vue.component('navbar', require('./components/manage/elements/navbar').default);
Vue.component('calculator', require('./components/manage/elements/calculator').default);
Vue.component('querybuilder', require('./components/manage/elements/querybuilder').default);
Vue.component('inventory', require('./components/manage/elements/inventory').default);
Vue.component('products', require('./components/manage/products').default);
Vue.component('users', require('./components/manage/users/index').default);
Vue.component('users-create', require('./components/manage/users/create').default);
Vue.component('users-show', require('./components/manage/users/show').default);
Vue.component('categories', require('./components/manage/categories/index').default);
Vue.component('orders', require('./components/manage/orders/index').default);
Vue.component('edit-order', require('./components/manage/orders/edit').default);
Vue.component('edit-product', require('./components/manage/products/edit').default);
Vue.component('create-product', require('./components/manage/products/create').default);
Vue.component('datatable', require('./components/manage/elements/datatable').default);
Vue.component('rules', require('./components/manage/rules/create').default);
Vue.component('slider', require('./components/manage/sliders/edit').default);
Vue.component('categorytree', require('./components/elements/categorytree').default);
// Vue.component('testcomp', require('./components/testcomp').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
});
