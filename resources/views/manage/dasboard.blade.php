@extends('layouts.app')

@section('content')

     <dashboard :orders="{{json_encode($orders)}}" :lastorders="{{json_encode($lastOrders)}}"></dashboard>
@endsection

@section("page_scripts")
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

@endsection
