@extends('layouts.app')

@section('content')
    <datatable pageTitle='Παραγγελίες' :edit_url="{{json_encode($edit_url)}}" :data_url="{{json_encode($data_url)}}" :header="{{json_encode($headers)}}"></datatable>
@endsection

