@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        {!! Form::open(['route' => ['pages.update', $page->id], 'method' => 'put']) !!}
        <div class="row">
            <div class="col-md-12 text-right">
                <!--begin::Portlet-->

                <label>
                    <input type="checkbox" checked="checked" name="">
                    <span>@lang('pages.Active')</span>
                </label>
            </div>

            <div class="col-md-12">
                <div class="panel">

                   <div class="row">
                       <div class="col-md-8">
                           <div class="form-group ">
                               <label for="exampleInputEmail1">
                                   @lang('pages.Page_Title')
                               </label>
                               {!! Form::text('title', $page->title, ['class' => 'form-control m-input m-input--air']) !!}
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group ">
                               <label for="exampleInputEmail1">
                                   @lang('pages.Url')
                               </label>
                               {!! Form::text('slug', $page->slug, ['class' => 'form-control m-input m-input--air']) !!}
                           </div>
                       </div>
                   </div>
                    <div class="form-group ">

                        {!! Form::textarea('body', $page->body, ['class' => 'summernote ']) !!}

                    </div>

                    <div class="form-group text-right">
                        {!! Form::submit(__('pages.Update'), ['class' => ' btn btn-success ']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        <!--begin::Form-->
        <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route'=>['pages.destroy', $page->id], 'method'=>'delete']) !!}
                    {!!Form::submit(__('pages.DELETE'), ['class'=>'btn btn-danger'])!!}
                    {!! Form::close() !!}
                </div>
            </div>

    </div>


@endsection
@section("extra_style")
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
@endsection
@section("page_scripts")

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/lang/summernote-el-GR.js"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                height: 300,
                minHeight: null,
                maxHeight: null,
                focus: true,
                codemirror: {
                theme: 'monokai',
                    theme: 'default',
                    lineNumbers: true,
                    gutter: true,
                    lineWrapping: true
                },

                lang: 'el-GR'
            });
        });
    </script>
@endsection
