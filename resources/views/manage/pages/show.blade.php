
@extends('layouts.orange', ['title' => 'dashboard'])
@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                {!! Form::open(['route' => ['pages.update', $page->id], 'method' => 'put']) !!}
                <div class="m-portlet">


                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-map-location"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    @lang('orders.Edit_Page')
                                </h3>
                            </div>
                        </div>


                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <div class="m-portlet__nav-link m-portlet__nav-link--icon">
                                   <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                        <label>
                                            <input type="checkbox" checked="checked" name="">
                                            <span></span>
                                        </label>
                                    </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="m-portlet m-portlet--tab">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>

                                        <div class="form-group m-form__group row">
                                            <label for="exampleInputEmail1">
                                                @lang('pages.Page_Title')
                                            </label>
                                            {!! Form::text('title', $page->title, ['class' => 'form-control m-input m-input--air']) !!}
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <label for="exampleInputEmail1">
                                                @lang('pages.Url')
                                            </label>
                                            {!! Form::text('slug', $page->slug, ['class' => 'form-control m-input m-input--air']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Form-->


                            <div class="form-group m-form__group">
                                <label for="exampleTextarea">
                                    @lang('pages.Body')
                                </label>
                                {!! Form::textarea('body', $page->body, ['class' => 'summernote form-control m-input']) !!}

                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">

                                {!! Form::submit(__('pages.Update'), ['class' => 'form-control btn btn-brand']) !!}
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!--end::Portlet-->
        </div>
        

    </div>


@endsection
@section("page_scripts")


    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
    <script src="{{asset('assets/manage/js/summernote.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/manage/js/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
@endsection