@extends('layouts.app')

@section('content')
    <edit-product :calculatorquery="{{$calculator}}" :productcategories="{{$productCategories}}" :product="{{$product}}"></edit-product>
@endsection

