@extends('layouts.app')

@section('content')
    <rules :edit="{{json_encode($rule)}}" :query="{{json_encode($rule)}}"></rules>
@endsection
