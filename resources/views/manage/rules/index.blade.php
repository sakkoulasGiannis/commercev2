@extends('layouts.app')

@section('content')
    <datatable method='rules' :data_url="{{json_encode($data_url)}}" :header="{{json_encode($headers)}}" :edit_url="{{json_encode($edit_url)}}"></datatable>
@endsection

