@extends('layouts.app')

@section('content')
    <datatable method='sliders' :data_url="{{json_encode($data_url)}}" :header="{{json_encode($headers)}}"></datatable>
@endsection

