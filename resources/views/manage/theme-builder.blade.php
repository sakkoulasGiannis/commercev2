<link href="https://unpkg.com/grapesjs/dist/css/grapes.min.css" rel="stylesheet"/>
<script src="https://unpkg.com/grapesjs"></script>
<script src="/js/grapesjs-blocks-bootstrap4.min.js"></script>

<div id="gjs"></div>

<script type="text/javascript">


    var editor = grapesjs.init({
        container : '#gjs',

        plugins: ['grapesjs-blocks-bootstrap4', 'customPlugin'],
        pluginsOpts: {
        'grapesjs-blocks-bootstrap4': {
            blocks: {

            },
            blockCategories: {
                // ...
            },
            labels: {
                // ...
            },
            // ...
        }
    },
    canvas: {
        styles: [
            'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css'
        ],
            scripts: [
            'https://code.jquery.com/jquery-3.3.1.slim.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
            'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'
        ],
    }
    });

    var blockManager = editor.BlockManager;


    blockManager.add('some-block-id', {
        label: 'Products',
        content: "<div class='' > Kalhmera </div>",
        render: ({ model, className }) => `<div class="${className}__my-wrap">
      Before label
      ${model.get('label')}
      After label
    </div>`,
    });



</script>
