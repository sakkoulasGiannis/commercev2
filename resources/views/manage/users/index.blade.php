@extends('layouts.app')

@section('content')
    <datatable method='users' pageTitle='Παραγγελίες' :edit_url="{{json_encode($edit_url)}}" :data_url="{{json_encode($data_url)}}" :header="{{json_encode($headers)}}"></datatable>
@endsection

