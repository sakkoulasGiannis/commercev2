@extends('layouts.app')

@section('content')
    <users-show :user="{{json_encode($user)}}"></users-show>
@endsection

