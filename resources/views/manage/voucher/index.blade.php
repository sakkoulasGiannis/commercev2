@extends('layouts.app')

@section('content')
    <datatable method='vouchers' :data_url="{{json_encode($data_url)}}" :header="{{json_encode($headers)}}"></datatable>
@endsection

