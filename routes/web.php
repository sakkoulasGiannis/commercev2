<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// @todo admin login page
//Route::get('/manage/login', 'Auth\AdminLoginController@showLoginForm')->name('manage.login');
//Route::post('/manage/login', 'Auth\AdminLoginController@login')->name('manage.login.submit');
//Route::get('/manage/logout', 'Auth\AdminLoginController@logout')->name('manage.logout');


Route::group(['prefix' => 'manage', 'middleware' => ['role:admin']], function () {
    Route::get('theme-builder', 'ThemeController@index');
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/search', 'DashboardController@dashboardSearch')->name('categories.json');
    //    fields
    Route::get('/fields', 'FieldController@index')->name('fields');
    Route::get('/fields/get_fields', 'FieldController@get_fields')->name('get_fields');
    Route::post('/fields/add_field_attribute', 'FieldController@add_field_attribute')->name('add_field_attribute');
    Route::post('/fields/get_field_values', 'FieldController@get_field_values')->name('get_fields');
    Route::post('/fields/get_field_values_all', 'FieldController@get_field_values_all')->name('get_fields_values_all');
    //   end fields
    //    products
    Route::get('/products', 'ProductController@index')->name('products');
    Route::get('/products/create', 'ProductController@create')->name('products.create');
    Route::post('/products/store', 'ProductController@store')->name('products.store');
    Route::get('/products/{product}/get_stock', 'ProductController@get_stock')->name('get_product_stock');
    Route::post('/products/{product}/update', 'ProductController@update')->name('product_update');
    Route::post('/products/{product}/stock/update', 'ProductController@stockUpdate')->name('product_stock_update');
    Route::post('/products/{product}/categories/update', 'ProductController@updateCategories')->name('product_categories_update');
    Route::post('/products/{product}/calculator/update', 'ProductController@calculatorUpdate')->name('product_calculator_update');
    Route::post('/products/{product}/fields/update', 'ProductController@fieldsUpdate')->name('product_fields_update');
    Route::get('/products/{product}/media', 'ProductController@getMedia')->name('product.get.media');
    Route::post('/products/{product}/media', 'ProductController@addMedia')->name('product.add.media');
    Route::post('/media/{id}/delete', 'ProductController@deleteMedia')->name('product.delete.media');
    Route::post('/products/search', 'ProductController@search')->name('product.search');
    Route::get('/products/{product}', 'ProductController@edit')->name('product_edit');
    Route::get('/products_list_json', 'ProductController@listJson')->name('products');
    Route::post('/product/addStockVariation', 'StockVariationController@create')->name('add.stock.variation');
    Route::post('/products/{product}/destroy', 'ProductController@destroy')->name('product.destroy');
    Route::post('/product/destory_stock_variation', 'StockVariationController@destroy')->name('destory.stock.variation');
    Route::post('/product/destory_stock', 'StockController@destroy')->name('destory.stock');
    Route::get('/product/elastic', 'ProductController@elastic');

//  end products

    Route::get('/rules/query', 'RuleController@query')->name('rules.query');
    Route::resource('/rules', 'RuleController');
    Route::get('/rules_list_json', 'RuleController@listJson')->name('categories.json');
    Route::post('/rules/filter_products', 'RuleController@filter_products')->name('filter_products');
//    pages
    Route::resource('/pages', 'PageController');
    Route::get('/pages_list_json', 'PageController@listJson')->name('categories.json');
//    end pages

//    vouchers
    Route::resource('/vouchers', 'VoucherController');
    Route::get('/pages_list_json', 'VoucherController@listJson')->name('vouchers.json');
//    end vouchers


    //    vouchers
    Route::get('/sliders/{slider}/images', 'SliderController@getImages');
    Route::resource('/sliders', 'SliderController');
    Route::post('/sliders/{slider}/image', 'SliderController@addPhoto');
    Route::get('/sliders/{image}/destroy', 'SliderController@destroyPhoto');
    Route::get('/slider_list_json', 'SliderController@listJson')->name('vouchers.json');
//    end vouchers

//    pages
    Route::resource('/users', 'UserController');
    Route::get('/users_list_json', 'UserController@listJson')->name('categories.json');
//    end pages

//    Categories
    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/categories_list_json', 'CategoryController@categoriesTree')->name('categories.json');
    Route::post('/categories/create', 'CategoryController@create')->name('category.create');
    Route::post('/categories/store', 'CategoryController@index')->name('category.store');
    Route::post('/categories/{category}/update', 'CategoryController@update')->name('category');
    Route::get('/categories/{category}', 'CategoryController@category')->name('category');
    Route::get('/get_categories_for_query/', 'CategoryController@get_categories_for_query')->name('get_categories_for_query');
//    end Categories

//    Orders
    Route::get('/orders', 'OrderController@index')->name('orders');
    Route::get('/orders_list_json', 'OrderController@listJson')->name('orders.json');
    Route::get('/orders/create', 'OrderController@index')->name('orders.create');
    Route::post('/orders/store', 'OrderController@index')->name('orders.store');
    Route::get('/orders/{order}', 'OrderController@edit')->name('orders.edit');
//    end orders

    Route::get('/reports/import', 'ReportController@import')->name('orders.import');
    Route::post('/reports/import-csv', 'ReportController@importCsv')->name('orders.import.csv');
    Route::get('/reports/export', 'ReportController@export')->name('orders.export');


//    Route::get('dashboard', function(){
//        return view('manage.dasboard');
//    });
});

Route::prefix('/cart')->group(function () {
    Route::get('/', 'CartController@index')->name('cart');
    Route::get('/items', 'CartController@cartItems')->name('cart.items');
    Route::get('/checkout', 'CartController@checkout')->name('cart.checkout');
    Route::post('/checkout', 'CartController@Complete')->name('cart.checkout.complete');
    Route::post('/checkout/payments', 'CartController@payments')->name('cart.checkout.payments');
    Route::post('/flush/{id}', 'CartController@delete')->name('cart.deleteItem');

    Route::get('/checkout/success', 'CartController@checkoutWithStatus')->name('checkout.success');
    Route::get('/checkout/complete', 'CartController@completed')->name('checkout.completed');
    Route::get('/checkout/cancel', 'CartController@checkoutWithStatus');
    Route::get('/checkout/error', 'CartController@checkoutWithStatus')->name('checkout.error');

});
Route::match(['GET', 'POST'], '/search', 'SearchController@search')->name('search');
Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');
Route::post('/add_to_cart', 'CartController@add')->name('cart.add');
Route::post('/add_to_cart_dynamic_product', 'CartController@addDynamic')->name('cart.add.dynamic');
Route::post('/add_voucher', 'CartController@add_voucher')->name('cart.add.voucher');
//Route::post('/policy/accept', 'GdprController@aprove')->name('policy.accept');
Route::post('/calculator_get_price', 'CartController@dynamic_price')->name('dynamic_price');
Route::get('development', 'TestController@test');
Route::get('search', function() {
    $query = 'γόβα'; // <-- Change the query for testing.

    $products = App\Product::search($query)->get();

    return $products;
});

Route::get('/search_algolia', 'ProductController@searchAlgolia');


Route::match(['GET', 'POST'], 'en/{path?}/', 'SegmentController@getCat')->where('path', '[a-zA-Z0-9/._-]+');
Route::match(['GET', 'POST'], '/{path?}/', 'SegmentController@getCat')->where('path', '[a-zA-Z0-9/._-]+');


//Route::filter('pattern: *', array('name' => 'langredirect', function()
//{
//    $uri = Request::server('request_uri');
//    $segments = explode('/', $uri);
//    if ( ! array_get(Config::get('application.languages'), $segments[1]) )
//    {
//        return Redirect::to(URL::base() . '/' . Config::get('application.language') . $uri);
//    }
//}));
